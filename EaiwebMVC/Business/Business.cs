﻿using EaiwebMVC.DAL;
using EaiwebMVC.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EaiwebMVC.Business
{
    public class Business
    {
        public static readonly Business _instance = new Business();

        public async Task<UserRole> GetRole(string user)
        {
            var ur = new UserRole();
            var obj = await DataAccessLayer._instance.UserRole(user);
            string role = obj.ds.Tables[0].Rows[0]["EmployeeRoleID"].ToString();

            switch (role)
            {
                case "TC":
                    ur.isCC = false;
                    ur.isDPS = false;
                    ur.isRSA = false;
                    ur.isDGCDAQSAE = false;
                    ur.isDenied = false;
                    break;
                case "CC":
                    ur.isCC = true;
                    ur.isDPS = false;
                    ur.isRSA = false;
                    ur.isDGCDAQSAE = false;
                    ur.isDenied = false;
                    break;
                case "RAS":
                    ur.isCC = false;
                    ur.isDPS = false;
                    ur.isRSA = true;
                    ur.isDGCDAQSAE = false;
                    ur.isDenied = false;
                    break;
                case "DPS":
                    ur.isCC = false;
                    ur.isDPS = true;
                    ur.isRSA = false;
                    ur.isDGCDAQSAE = false;
                    ur.isDenied = false;
                    break;
                case "CDA":
                case "DG":
                case "QSAE":
                    ur.isCC = false;
                    ur.isDPS = false;
                    ur.isRSA = false;
                    ur.isDGCDAQSAE = true;
                    ur.isDenied = false;
                    break;
                default:
                    ur.isDenied = true;
                    break;
            }
            return ur;
        }

        public async Task<user> GetUserMeta(string user)
        {
            var obj = new user();
            obj = await DataAccessLayer._instance.UserMeta(user);
            return obj;
        }

        public async Task<AppNews> GetNews(string user)
        {
            AppNews an = new AppNews();
            try
            {
                var obj = await DataAccessLayer._instance.ListNews(user);
                var anClCount = obj[0].CLCount;
                if (anClCount != null)
                {
                    an.CLCount = obj[0].CLCount;
                }
                else
                {
                    an.CLCount = "0";
                }
            }
            catch (Exception e)
            {
                an.CLCount = "0";
                
            }

            return an;
        }

        public async Task<DataResponse> SendEmail(Mail m)
        {
            var obj = new DataResponse();
            var message = new MailMessage();
            try
            {
                MailAddress fromAddress = new MailAddress(m.Sender, m.DisplaySender);

                foreach (var address in m.Recipient.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.To.Add(address);
                }

                using (MemoryStream memoryStream = new MemoryStream(m.stream[0]))
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    Attachment attachment = new Attachment(memoryStream, m.AttachmentName[0]);

                    message.CC.Add(m.Sender);
                    message.From = fromAddress;
                    message.Priority = MailPriority.High;
                    message.IsBodyHtml = true;
                    message.Subject = m.Subject;
                    message.Body = m.Message;
                    message.Attachments.Add(attachment);
                    /* TODO: CE_CVF */
                    if (m.stream.Count > 1)
                    {
                        for (int i = 1; i < m.stream.Count; i++)
                        {
                            MemoryStream ms = new MemoryStream(m.stream[i]);
                            ms.Seek(0, SeekOrigin.Begin);
                            Attachment a = new Attachment(ms, m.AttachmentName[i]);
                            message.Attachments.Add(a);
                        }
                    }

                    using (var smtpClient = new SmtpClient("SMTPAPPS.CPL.LOC"))
                    {
                        /* TODO : test local email */
                        //smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                        await smtpClient.SendMailAsync(message);
                    }
                }
                obj.success = true;
            }
            catch (Exception e)
            {
                obj.success = false;
                obj.response = System.Text.RegularExpressions.Regex.Replace(e.ToString(), @"\t|\n|\r", " ");
            }
            return obj;
        }

        public byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        Business()
        {
        }
    }
}
