﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EaiwebMVC.Controllers
{
    public class FileUploadController : Controller
    {
        [HttpGet("FileUpload")]
        public IActionResult Upload()
        {
            return View();
        }

        [HttpPost("FileUpload")]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            var filePaths = new List<string>();
            foreach (var formFile in files)
            {
                // ************************************************************************
                if (formFile.Length > 0)
                {
                    // full path to file in temp location
                    var filePath = Path.GetTempFileName();
                    filePaths.Add(filePath);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
                //*************************************************************************


                /*
                                string nomeFileUpload = formFile.FileName;

                                if (nomeFileUpload.Length >= 51)
                                {
                                    lstFlussi.Visible = true;
                                    lstFlussi.BackColor = System.Drawing.Color.Red;
                                    lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " Nome File troppo lungo (> di 50 caratteri)");
                                }
                                else
                                if (!File.Exists(Path.Combine(ConfigurationManager.AppSettings["CartellaFattureInput"], Uploader1.Items[i].FileName)))
                                {
                                    // CONTROLLO ERRORE NOME FILE
                                    var nomeFile = Uploader1.Items[i].FileName;
                                    if (nomeFile.Split('_').Length == 7 &&
                                        nomeFile.Split('_')[2].Length == 1 &&
                                        (nomeFile.Split('_')[6].Split('.')[0].Length == 2 ||
                                        nomeFile.Split('_')[6].Split('.')[0].Length == 3) &&
                                        nomeFile.Split('_')[2].Equals("T") == false &&
                                        ((nomeFile.EndsWith(".TXT") == true || nomeFile.EndsWith(".XML") == true)) &&
                                        nomeFile.Trim().Contains(" ") == false)
                                    {
                                        // CONTROLLO PROGRESSIVO FINALE FILE
                                        string nomefile = Uploader1.Items[i].FileName;

                                        string nomefile_senza_prog = nomefile.Substring(0, nomefile.Length - 7);

                                        string test_societa = nomeFile.Split('_')[0];
                                        string test_prog = nomeFile.Split('_')[5];

                                        int prog_finale = Convert.ToInt32(nomefile.Split('_')[6].Split('.')[0]);

                                        bool errore_progressivo_finale = false;
                                        if (prog_finale > 1)
                                        {
                                            int num_file = 0;
                                            for (int x = 0; x < Uploader1.Items.Count; x++)
                                            {
                                                string upload_societa = Uploader1.Items[x].FileName.Split('_')[0];
                                                string upload_prog = Uploader1.Items[x].FileName.Split('_')[5];

                                                if (upload_societa.Equals(test_societa) && upload_prog.Equals(test_prog))
                                                {
                                                    num_file++;
                                                }
                                            }
                                            if (num_file < prog_finale)
                                            {
                                                errore_progressivo_finale = true;
                                            }
                                        }

                                        if (!errore_progressivo_finale)
                                        {
                                            // CONTROLLO DOPPIO FLUSSO
                                            string ConnectionString = ConfigurationManager.ConnectionStrings["EAIWEB"].ConnectionString;

                                            string CommandText = "_check_erroriflusso";
                                            SqlConnection myConnection = new SqlConnection(ConnectionString);
                                            SqlCommand myCommand = new SqlCommand(CommandText, myConnection);
                                            myCommand.CommandType = CommandType.StoredProcedure;

                                            SqlParameter filefatParam = myCommand.Parameters.Add("@filefat", SqlDbType.NVarChar, 100);
                                            SqlParameter controllo_1 = myCommand.Parameters.Add("@controllo_1", SqlDbType.NVarChar, 100);
                                            SqlParameter controllo_2 = myCommand.Parameters.Add("@controllo_2", SqlDbType.NVarChar, 100);
                                            SqlParameter controllo_3 = myCommand.Parameters.Add("@controllo_3", SqlDbType.NVarChar, 100);
                                            SqlParameter nometxtParam = myCommand.Parameters.Add("@nometxt", SqlDbType.NVarChar, 50);
                                            filefatParam.Value = Uploader1.Items[i].FileName;


                                            string[] temp_strings = Uploader1.Items[i].FileName.Split('_');
                                            controllo_1.Value = temp_strings[0] + "_";
                                            controllo_2.Value = "_" + temp_strings[2] + "_" + temp_strings[3] + "_";
                                            //controllo_2.Value = "_" + temp_strings[3] + "_";
                                            controllo_3.Value = "_" + temp_strings[5] + "_" + temp_strings[6];


                                            nometxtParam.Value = Uploader1.Items[i].FileName.Split('_')[0];

                                            myConnection.Open();
                                            SqlDataReader reader = myCommand.ExecuteReader();

                                            while (reader.Read())
                                            {
                                                if (reader[0].ToString().Equals("1"))
                                                {
                                                    // FLUSSO NON PRESENTE CONTINUA CON IL CARICAMENTO
                                                    reader.Close();

                                                    if (nomeFile.Split('_')[2].Equals("Z") &&
                                                        Uploader1.Items[i].FileName.Split('_')[0].Equals("EMILIAM"))
                                                    {
                                                        // Gestione particolare di emiliaambiente
                                                        File.Copy(Uploader1.Items[i].GetTempFilePath(),
                                                            CombinePaths(ConfigurationManager.AppSettings["CartellaFattureInput"], "EMILIA", Uploader1.Items[i].FileName), true);

                                                        lstFlussi.Visible = true;
                                                        lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " caricato correttamente!");

                                                        CommandText = "_insert_upload";
                                                        myCommand = new SqlCommand(CommandText, myConnection);
                                                        myCommand.CommandType = CommandType.StoredProcedure;

                                                        SqlParameter myParm1 = myCommand.Parameters.Add("@filefat", SqlDbType.NVarChar, 100);
                                                        SqlParameter myParm2 = myCommand.Parameters.Add("@societa", SqlDbType.NVarChar, 50);
                                                        SqlParameter myParm3 = myCommand.Parameters.Add("@utente", SqlDbType.NVarChar, 50);
                                                        SqlParameter myParm4 = myCommand.Parameters.Add("@prog", SqlDbType.NVarChar, 50);
                                                        myParm1.Value = Uploader1.Items[i].FileName;
                                                        myParm2.Value = Uploader1.Items[i].FileName.Split('_')[0];
                                                        myParm3.Value = User.Identity.Name;
                                                        myParm4.Value = Uploader1.Items[i].FileName.Split('_')[5];

                                                        myCommand.ExecuteReader();
                                                    }
                                                    else if (nomeFile.Split('_')[2].Equals("S"))
                                                    {
                                                        // Gestione particolare per i flussi delle bollette da inviare allo SDI
                                                        // NB a ogni modo il fatturatore può caricare un flusso di questo tipo (anche se la società non gestisce gli esiti)
                                                        //	  sarà poi la console a rigettare lo stesso flusso, inviando una mail al fatturatore

                                                        File.Copy(Uploader1.Items[i].GetTempFilePath(),
                                                            Path.Combine(ConfigurationManager.AppSettings["CartellaFattureInput"], Uploader1.Items[i].FileName));

                                                        lstFlussi.Visible = true;
                                                        lstFlussi.Items.Add("Flusso (SDI): " + Uploader1.Items[i].FileName + " caricato correttamente!");

                                                        //FS cambio solamente la stored che mi permette di inserire flussi con ID specifico per l'invio allo SDI
                                                        CommandText = "_insert_upload_SDI";
                                                        myCommand = new SqlCommand(CommandText, myConnection);
                                                        myCommand.CommandType = CommandType.StoredProcedure;

                                                        SqlParameter myParm1 = myCommand.Parameters.Add("@filefat", SqlDbType.NVarChar, 100);
                                                        SqlParameter myParm2 = myCommand.Parameters.Add("@societa", SqlDbType.NVarChar, 50);
                                                        SqlParameter myParm3 = myCommand.Parameters.Add("@utente", SqlDbType.NVarChar, 50);
                                                        SqlParameter myParm4 = myCommand.Parameters.Add("@prog", SqlDbType.NVarChar, 50);
                                                        myParm1.Value = Uploader1.Items[i].FileName;
                                                        myParm2.Value = Uploader1.Items[i].FileName.Split('_')[0];
                                                        myParm3.Value = User.Identity.Name;
                                                        myParm4.Value = Uploader1.Items[i].FileName.Split('_')[5];

                                                        myCommand.ExecuteReader();


                                                    }
                                                    else
                                                    {
                                                        // Gestione standard
                                                        File.Copy(Uploader1.Items[i].GetTempFilePath(),
                                                            Path.Combine(ConfigurationManager.AppSettings["CartellaFattureInput"], Uploader1.Items[i].FileName));

                                                        lstFlussi.Visible = true;
                                                        lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " caricato correttamente!");

                                                        CommandText = "_insert_upload";
                                                        myCommand = new SqlCommand(CommandText, myConnection);
                                                        myCommand.CommandType = CommandType.StoredProcedure;

                                                        SqlParameter myParm1 = myCommand.Parameters.Add("@filefat", SqlDbType.NVarChar, 100);
                                                        SqlParameter myParm2 = myCommand.Parameters.Add("@societa", SqlDbType.NVarChar, 50);
                                                        SqlParameter myParm3 = myCommand.Parameters.Add("@utente", SqlDbType.NVarChar, 50);
                                                        SqlParameter myParm4 = myCommand.Parameters.Add("@prog", SqlDbType.NVarChar, 50);
                                                        myParm1.Value = Uploader1.Items[i].FileName;
                                                        myParm2.Value = Uploader1.Items[i].FileName.Split('_')[0];
                                                        myParm3.Value = User.Identity.Name;
                                                        myParm4.Value = Uploader1.Items[i].FileName.Split('_')[5];

                                                        myCommand.ExecuteReader();
                                                    }
                                                }
                                                else if (reader[0].ToString().Equals("0"))
                                                {
                                                    lstFlussi.Visible = true;
                                                    lstFlussi.BackColor = System.Drawing.Color.Red;
                                                    lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " non caricato perchè già presente!");
                                                }
                                                else if (reader[0].ToString().Equals("2"))
                                                {
                                                    lstFlussi.Visible = true;
                                                    lstFlussi.BackColor = System.Drawing.Color.Red;
                                                    lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " non caricato perchè società non presente nel database!");
                                                }
                                                break;
                                            }

                                            myConnection.Close();
                                        }
                                        else
                                        {
                                            // Mostra l'errore
                                            lstFlussi.Visible = true;
                                            lstFlussi.BackColor = System.Drawing.Color.Red;
                                            lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " non caricato perchè c'è un errore nel progressivo finale del file!");
                                        }
                                    }
                                    else
                                    {
                                        if (nomeFile.Split('_')[2].Equals("T"))
                                        {
                                            lstFlussi.Visible = true;
                                            lstFlussi.BackColor = System.Drawing.Color.Red;
                                            lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " non caricato perchè \"T\" non è accettato come \"TIPO-CONSEGNA\"!");
                                        }
                                        else
                                        {
                                            // ERRORE NEL NOME DEL FILE
                                            lstFlussi.Visible = true;
                                            lstFlussi.BackColor = System.Drawing.Color.Red;
                                            lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " non caricato perchè c'è un errore nel nome del file!");
                                        }
                                    }
                                }
                                else
                                {
                                    lstFlussi.Visible = true;
                                    lstFlussi.BackColor = System.Drawing.Color.Red;
                                    lstFlussi.Items.Add("Flusso: " + Uploader1.Items[i].FileName + " non caricato perchè già presente!");
                                }


    */
            }


            // process uploaded files
            // Don't rely on or trust the FileName property without validation.
            return View();
            //return Ok(new { count = files.Count, size, filePaths });


        }

    }
}