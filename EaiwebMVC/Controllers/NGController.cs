﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EaiwebMVC.Models;
using System.Data;
using EaiwebMVC.DAL;
using System.Net;

namespace EaiwebMVC.Controllers
{
    public class NGController : Controller
    {
        public IActionResult WorkOrder()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {

                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("WorkOrder");
            }
            else
            {
                return View("AppError");
            }
        }
        public IActionResult WorkOrderList()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {

                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("WorkOrderList");
            }
            else
            {
                return View("AppError");
            }
        }
        public IActionResult WorkOrderArchive()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {

                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("WorkOrderArchive");
            }
            else
            {
                return View("AppError");
            }
        }
        public IActionResult WorkOrderRemi()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {

                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("WorkOrderRemi");
            }
            else
            {
                return View("AppError");
            }
        }
        public IActionResult Scheduler()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("Scheduler");
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult WorkOrderScheduler()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View();
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult Invoice()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("DraftInvoice");
            }
            else
            {
                return View("AppError");
            }
        }
        public IActionResult Planning()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("Planning");
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult WorkOrderPlan()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View();
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult WorkOrderCons()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View();
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult FinalBalance()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {
                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("FinalBalance");
            }
            else
            {
                return View("AppError");
            }
        }
    }
}
