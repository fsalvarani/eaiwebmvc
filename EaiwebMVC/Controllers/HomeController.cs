﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EaiwebMVC.Models;
using System.Data;
using EaiwebMVC.DAL;
using System.Net;

namespace EaiwebMVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CheckList()
        {
            string user = User.Identity.Name;
            if (Business.Business._instance.GetRole(user).Result.isCC)
            {
                return View("CheckList");
            }
            if (Business.Business._instance.GetRole(user).Result.isRSA)
            {
                return View("CheckList_RAS");
            }
            if (Business.Business._instance.GetRole(user).Result.isDPS)
            {
                return View("CheckList_DPS");
            }
            if (Business.Business._instance.GetRole(user).Result.isDGCDAQSAE)
            {
                return View("CheckList_DGCDAQSAE");
            }
            else
            {
                return View("AppError");
            }

        }

        public IActionResult Summary()
        {
            user My = new user()
            {
                username = User.Identity.Name
            };
            ViewData["MyUser"] = My;
            if (Business.Business._instance.GetRole(My.username).Result.isCC)
            {
                return View("AppError");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isRSA)
            {
                return View("Summary_RAS");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isDPS)
            {
                return View("Summary_DPS");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isDGCDAQSAE)
            {
                return View("Summary_DGCDAQSAE");
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult Report()
        {
            user My = new user()
            {
                username = User.Identity.Name
            };
            ViewData["MyUser"] = My;
            if (Business.Business._instance.GetRole(My.username).Result.isCC)
            {
                return View("AppError");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isRSA)
            {
                return View("Report_RSA");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isDPS)
            {
                return View("Report_DPS");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isDGCDAQSAE)
            {
                return View("Report_DGCDAQSAE");
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult Final()
        {
            user My = new user()
            {
                username = User.Identity.Name
            };
            ViewData["MyUser"] = My;
            if (Business.Business._instance.GetRole(My.username).Result.isCC)
            {
                return View("AppError");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isRSA)
            {
                return View("AppError");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isDPS)
            {
                return View("AppError");
            }
            if (Business.Business._instance.GetRole(My.username).Result.isDGCDAQSAE)
            {
                return View("Final_DGCDAQSAE");
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult Odl()
        {
            var obj = Business.Business._instance.GetUserMeta(User.Identity.Name);
            user My = new user()
            {

                username = obj.Result.username,
                uid = obj.Result.uid,
                sector = obj.Result.sector
            };
            ViewData["MyUser"] = My;
            if (My.sector == "NG")
            {
                return View("Odl");
            }
            else
            {
                return View("AppError");
            }
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
