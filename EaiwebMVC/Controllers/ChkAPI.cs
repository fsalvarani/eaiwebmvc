﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using EaiwebMVC.Models;
using System.Threading.Tasks;
using System.Data;
using System;
using EaiwebMVC.DAL;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EaiwebMVC.Controllers
{
    #region CHECKAPP_APIS

    [Route("api/[controller]")]
    public class WebApiNgActivities : Controller
    {
        public async Task<IList<NgActivity>> nglist()
        {
            var obj = await DataAccessLayer._instance.NGActivities();
            return obj;
        }
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<NgActivity>> GetAllBooks()
        {
            IList<NgActivity> x = await nglist();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<NgActivity> x = await nglist();
            var books = x.Where((p) => p.Odl == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiNgActivitiesStat : Controller
    {
        public async Task<IList<NgActivityStat>> ngstat()
        {
            var obj = await DataAccessLayer._instance.NGActivitiesStat();
            return obj;
        }
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<NgActivityStat>> GetAllBooks()
        {
            IList<NgActivityStat> x = await ngstat();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<NgActivityStat> x = await ngstat();
            var books = x.Where((p) => p.ActivitiesDescription == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetImgWOList : Controller
    {
        public async Task<IList<WorkOrderImgWFMModel>> imgwfmlist(int odl)
        {
            var obj = await DataAccessLayer._instance.WorkOrderImgListWFM(odl);
            return obj;
        }
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderImgWFMModel>> GetAllBooks(int odl)
        {
            IList<WorkOrderImgWFMModel> x = await imgwfmlist(odl);
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WorkOrderImgWFMModel> x = await imgwfmlist(id);
            var books = x.Where((p) => p.Odl == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWOList : Controller
    {
        public async Task<IList<WorkOrderWFMModel>> wowfmlist()
        {
            var obj = await DataAccessLayer._instance.WorkOrderListWFMtest();
            return obj;
        }
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderWFMModel>> GetAllBooks()
        {
            IList<WorkOrderWFMModel> x = await wowfmlist();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(DateTime id)
        {
            IList<WorkOrderWFMModel> x = await wowfmlist();
            var books = x.Where((p) => p.OpenDate == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            var j = JsonConvert.DeserializeObject<WorkOrderClosing>(value.ToString());
            var obj = await DataAccessLayer._instance._CloseWorkOrder(j, User.Identity.Name);
            if (obj.success == true)
            {
                resp = new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
            }
            return resp;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWOData : Controller
    {
        public async Task<IList<WorkOrderWFMModel>> wowfmlist(DateTime from, DateTime to)
        {
            var obj = await DataAccessLayer._instance.WorkOrderListWFM(from,to);
            return obj;
        }
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderWFMModel>> GetAllBooks(DateTime from, DateTime to)
        {
            IList<WorkOrderWFMModel> x = await wowfmlist(from,to);
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(DateTime from, DateTime to)
        {
            IList<WorkOrderWFMModel> x = await wowfmlist(from,to);
            var books = x.Where((p) => p.OpenDate == from);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            var j = JsonConvert.DeserializeObject<WorkOrderClosing>(value.ToString());
            var obj = await DataAccessLayer._instance._CloseWorkOrder(j, User.Identity.Name);
            if (obj.success == true)
            {
                resp = new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
            }
            return resp;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWOListA : Controller
    {
        public async Task<IList<WorkOrderWFMModel>> wowfmlistA()
        {
            var obj = await DataAccessLayer._instance.WorkOrderListWFMA();
            return obj;
        }
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderWFMModel>> GetAllBooks()
        {
            IList<WorkOrderWFMModel> x = await wowfmlistA();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WorkOrderWFMModel> x = await wowfmlistA();
            var books = x.Where((p) => p.Odl == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            var j = JsonConvert.DeserializeObject<WorkOrderClosing>(value.ToString());
            var obj = await DataAccessLayer._instance._CloseWorkOrder(j, User.Identity.Name);
            if (obj.success == true)
            {
                resp = new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
            }
            return resp;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }

    [Route("api/[controller]")]
    public class WebApiGetPriceList : Controller
    {
        public async Task<IList<PriceListrModel>> plist()
        {
            var obj = await DataAccessLayer._instance.PriceList();
            return obj;
        }
        // GET api/values/5
        [HttpGet("{wbs}/{customer}")]
        public async Task<IActionResult> Get(string wbs, int customer)
        {
            IList<PriceListrModel> x = await plist();
            var books = x.Where((p) => p.Wbs == wbs && p.CustomerId == customer);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

    }

    [Route("api/[controller]")]
    public class WebApiGetRemi : Controller
    {
        public async Task<IList<RemiModel>> GetAllRemi()
        {
            var obj = await DataAccessLayer._instance.RemiList();
            return obj;
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetPlanning : Controller
    {
        public async Task<IList<WorkOrderPlanningModel>> planning()
        {
            var obj = await DataAccessLayer._instance.WorkOrderPlanning();
            return obj;
        }
        //// GET api/values/5
        //[HttpGet("{dataFrom}/{dataTo}/{customer}/{wbs}")]
        //public async Task<IActionResult> Get(DateTime dataFrom, DateTime dataTo, string customer, string wbs)
        //{
        //    IList<WorkOrderPlanningModel> x = await planning();
        //    var books = x.Where((p) => p.CustomerID == customer && (p.ExecutionDate >= dataFrom && p.ExecutionDate <= dataTo) && p.Wbs == wbs);

        //    var item = books;
        //    if (item == null)
        //    {
        //        return NotFound();
        //    }
        //    return new ObjectResult(item);
        //}

    }
    [Route("api/[controller]")]
    public class WebApiGetFinalBalance : Controller
    {
        [HttpGet]
        public async Task<IList<WorkOrderBalanceModel>> cons()
        {
            var obj = await DataAccessLayer._instance.WorkOrderCons();
            return obj;
        }

        [HttpGet("{from}/{to}")]
        public async Task<IList<WorkOrderBalanceModel>> cons(DateTime from, DateTime to)
        {
            var obj = await DataAccessLayer._instance.WorkOrderCons(from, to);
            return obj;
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkOrderScheduler : Controller
    {
        public async Task<IList<WorkOrderModel>> workorder()
        {
            //var obj = await DataAccessLayer._instance.c();
            var obj = await DataAccessLayer._instance.WorkOrderList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderModel>> GetAllBooks()
        {
            IList<WorkOrderModel> x = await workorder();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<WorkOrderModel> x = await DataAccessLayer._instance.WorkOrderList(id);
            var books = x.Where((p) => p.WOElement == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        //[HttpPost]
        //public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        //{
        //    HttpResponseMessage y = null;
        //    var resp = y;
        //    var j = JsonConvert.DeserializeObject<WorkOrderModel>(value.ToString());
        //    var obj = await DataAccessLayer._instance._InsertWorkOrder(j, j.CreatedBy);
        //    if (obj.success == true)
        //    {
        //        resp = new HttpResponseMessage(HttpStatusCode.OK);
        //    }
        //    else
        //    {
        //        resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
        //    }
        //    return resp;
        //}

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetUserData : Controller
    {
        [HttpGet("{user}")]
        public async Task<user> usermetadata(string user)
        {
            var obj = await DataAccessLayer._instance.UserMeta(user);
            return obj;
        }
    }

    [Route("api/[controller]")]
    public class WebApiWbs : Controller
    {
        public async Task<IList<WbsrModel>> wbs()
        {
            var obj = await DataAccessLayer._instance.WbsList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WbsrModel>> GetAllBooks()
        {
            IList<WbsrModel> x = await wbs();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WbsrModel> x = await wbs();
            var books = x.Where((p) => p.CustomerId == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiSector : Controller
    {
        public async Task<IList<SectorModel>> sector()
        {
            var obj = await DataAccessLayer._instance.SctorTypeList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<SectorModel>> GetAllBooks()
        {
            IList<SectorModel> x = await sector();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<SectorModel> x = await sector();
            var books = x.Where((p) => p.SectorType == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkOrder : Controller
    {
        public async Task<IList<WorkOrderModel>> workorder()
        {
            //var obj = await DataAccessLayer._instance.c();
            var obj = await DataAccessLayer._instance.WorkOrderList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderModel>> GetAllBooks()
        {
            IList<WorkOrderModel> x = await workorder();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<WorkOrderModel> x = await DataAccessLayer._instance.WorkOrderList(id);
            var books = x.Where((p) => p.WOElement == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            var j = JsonConvert.DeserializeObject<WorkOrderModel>(value.ToString());
            var obj = await DataAccessLayer._instance._InsertWorkOrder(j, j.CreatedBy);
            if (obj.success == true)
            {
                resp = new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
            }
            return resp;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkOrderDetails : Controller
    {
        //public async Task<IList<WorkOrderDetails>> workorderdetails(int odl)
        //{
        //    var obj = await DataAccessLayer._instance.WorkOrderDetails(odl);
        //    return obj;
        //}

        // GET: api/values
        //[HttpGet]
        //public async Task<IEnumerable<WorkOrderDetails>> GetAllBooks()
        //{
        //    IList<WorkOrderDetails> x = await workorderdetails();
        //    return x;
        //}

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WorkOrderDetails> x = await DataAccessLayer._instance.WorkOrderDetails(id);
            var books = x.Where((p) => p.woid == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            var j = JsonConvert.DeserializeObject<WorkOrderDetails>(value.ToString());
            //var obj = await DataAccessLayer._instance._InsertWorkOrder(j);
            //if (obj.success == true)
            //{
            //    resp = new HttpResponseMessage(HttpStatusCode.OK);
            //}
            //else
            //{
            //    resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
            //}
            return resp;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkOrder4Invoice : Controller
    {
        public async Task<IList<WorkOrderInvoiceModel>> woinvoice()
        {
            var obj = await DataAccessLayer._instance.WorkOrder4InvoiceList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderInvoiceModel>> GetAllBooks()
        {
            IList<WorkOrderInvoiceModel> x = await woinvoice();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<WorkOrderInvoiceModel> x = await woinvoice();
            var books = x.Where((p) => p.ic == id);

            var item = books;
            return new ObjectResult(item);
        }

        // POST api/values
        //[HttpPost]
        //public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        //{
        //HttpResponseMessage y = null;
        //var resp = y;
        //var j = JsonConvert.DeserializeObject<WorkOrderInvoiceModel>(value.ToString());
        //var obj = await DataAccessLayer._instance._InsertWorkOrder(j, j.CreatedBy);
        //if (obj.success == true)
        //{
        //    resp = new HttpResponseMessage(HttpStatusCode.OK);
        //}
        //else
        //{
        //    resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
        //}
        //return resp;
        //}

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkOrderType : Controller
    {
        public async Task<IList<WorkOrderTypeModel>> workordertype()
        {
            var obj = await DataAccessLayer._instance.WorkOrderTypeList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderTypeModel>> GetAllBooks()
        {
            IList<WorkOrderTypeModel> x = await workordertype();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WorkOrderTypeModel> x = await workordertype();
            var books = x.FirstOrDefault((p) => p.WorkOrderTypeID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkOrderStatus : Controller
    {
        public async Task<IList<WorkOrderStatusModel>> workorderstatus()
        {
            var obj = await DataAccessLayer._instance.WorkOrderStatusList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkOrderStatusModel>> GetAllBooks()
        {
            IList<WorkOrderStatusModel> x = await workorderstatus();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WorkOrderStatusModel> x = await workorderstatus();
            var books = x.FirstOrDefault((p) => p.WorkOrderStatusID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetWorkMan : Controller
    {
        public async Task<IList<WorkmanModel>> workman()
        {
            var obj = await DataAccessLayer._instance.WorkmanList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<WorkmanModel>> GetAllBooks()
        {
            IList<WorkmanModel> x = await workman();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<WorkmanModel> x = await workman();
            var books = x.Where((p) => p.WorkmanID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiCustomer : Controller
    {
        public async Task<IList<CustomerModel>> customer()
        {
            var obj = await DataAccessLayer._instance.ListCustomer();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<CustomerModel>> GetAllBooks()
        {
            IList<CustomerModel> x = await customer();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<CustomerModel> x = await customer();
            var books = x.FirstOrDefault((p) => p.CustomerID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiCustomerEnrollment : Controller
    {
        public async Task<IList<CustomerModel>> customer()
        {
            var obj = await DataAccessLayer._instance.ListCustomerEnrollment();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<CustomerModel>> GetAllBooks()
        {
            IList<CustomerModel> x = await customer();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            IList<CustomerModel> x = await customer();
            var books = x.FirstOrDefault((p) => p.CustomerID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetPlantbyCustomer : Controller
    {
        public async Task<IList<PlantModel>> plant()
        {
            var obj = await DataAccessLayer._instance.ListPlant("");
            return obj;
        }

        //GET: api/values
        [HttpGet]
        public async Task<IEnumerable<PlantModel>> GetAllBooks()
        {
            IList<PlantModel> x = await plant();
            return x;
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> Get(int id)
        //{
        //    IList<PlantModel> x = await plant();
        //    var books = x.Where((p) => p.CustomerID == id);

        //    var item = books;
        //    if (item == null)
        //    {
        //        return NotFound();
        //    }
        //    return new ObjectResult(item);
        //}

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIPlant : Controller
    {
        public async Task<IList<PlantModel>> plant()
        {
            var obj = await DataAccessLayer._instance.ListPlantCode();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<PlantModel>> GetAllBooks()
        {
            IList<PlantModel> x = await plant();
            return x;
        }

        [HttpGet("{type}/{value}")]
        public async Task<IList<PlantModel>> plant(string type, string value)
        {
            var obj = await DataAccessLayer._instance.ListPlant(type, value);
            return obj;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<PlantModel> x = await DataAccessLayer._instance.ListPlant(id);
            var books = x.FirstOrDefault((p) => p.ElementCode == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetICWBS : Controller
    {
        public IList<ICModel> ic()
        {
            var obj = DataAccessLayer._instance.ListIC();
            return obj.Result;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<ICModel> GetAllBooks()
        {
            IList<ICModel> x = ic();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            IList<ICModel> x = ic();
            var books = x.Where((p) => p.Wbs == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIIC : Controller
    {
        public IList<ICModel> ic()
        {
            var obj = DataAccessLayer._instance.ListIC();
            return obj.Result;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<ICModel> GetAllBooks()
        {
            IList<ICModel> x = ic();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<ICModel> x = ic();
            var books = x.Where((p) => p.CustomerID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPI : Controller
    {
        public IList<Model_CheckList4PM> CheckListDetails()
        {
            List<Model_CheckList4PM> cl = new List<Model_CheckList4PM>();
            var obj = DataAccessLayer._instance.CheckListPM(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new Model_CheckList4PM()
                {
                    CheckListID = int.Parse(dRow["CheckListHeaderID"].ToString()),
                    Timestamp = DateTime.Parse(dRow["Timestamp"].ToString()),
                    CheckListData = DateTime.Parse(dRow["CheckData"].ToString()),
                    CheckedBy = dRow["CheckedBy"].ToString(),
                    Wbs = dRow["Wbs"].ToString(),
                    WbsDescription = dRow["WbsDescription"].ToString(),
                    ProjectManager = dRow["ProjectManager"].ToString(),
                    Site = dRow["Site"].ToString(),
                    Sector = dRow["Sector"].ToString(),
                    Subcontractor = dRow["Subcontractor"].ToString()
                });
            }
            return cl;
        }
        public IList<Model_CheckList4PM> CheckListRAS()
        {
            List<Model_CheckList4PM> cl = new List<Model_CheckList4PM>();
            var obj = DataAccessLayer._instance.CheckListRAS(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new Model_CheckList4PM()
                {
                    CheckListID = int.Parse(dRow["CheckListHeaderID"].ToString()),
                    Timestamp = DateTime.Parse(dRow["Timestamp"].ToString()),
                    CheckListData = DateTime.Parse(dRow["CheckData"].ToString()),
                    CheckedBy = dRow["CheckedBy"].ToString(),
                    Wbs = dRow["Wbs"].ToString(),
                    WbsDescription = dRow["WbsDescription"].ToString(),
                    ProjectManager = dRow["ProjectManager"].ToString(),
                    Site = dRow["Site"].ToString(),
                    Subcontractor = dRow["Subcontractor"].ToString()
                });
            }
            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Model_CheckList4PM> GetAllBooks()
        {
            IList<Model_CheckList4PM> x = CheckListDetails();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<Model_CheckList4PM> x = CheckListDetails();
            var books = x.FirstOrDefault((p) => p.CheckListID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIRAS : Controller
    {
        public IList<Model_CheckList4PM> CheckListRAS()
        {
            List<Model_CheckList4PM> cl = new List<Model_CheckList4PM>();
            var obj = DataAccessLayer._instance.CheckListRAS(User.Identity.Name);
            if (obj.Result.isSuccess)
            {
                foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
                {
                    cl.Add(new Model_CheckList4PM()
                    {
                        CheckListID = int.Parse(dRow["CheckListHeaderID"].ToString()),
                        Timestamp = DateTime.Parse(dRow["Timestamp"].ToString()),
                        CheckListData = DateTime.Parse(dRow["CheckData"].ToString()),
                        CheckedBy = dRow["CheckedBy"].ToString(),
                        Wbs = dRow["Wbs"].ToString(),
                        WbsDescription = dRow["WbsDescription"].ToString(),
                        ProjectManager = dRow["ProjectManager"].ToString(),
                        Site = dRow["Site"].ToString(),
                        Subcontractor = dRow["Subcontractor"].ToString()
                    });
                }
            }

            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Model_CheckList4PM> GetAllBooks()
        {
            IList<Model_CheckList4PM> x = CheckListRAS();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<Model_CheckList4PM> x = CheckListRAS();
            var books = x.FirstOrDefault((p) => p.CheckListID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIDGCDAQSAE : Controller
    {
        public IList<Model_CheckList4PM> CheckListDGCDAQSAE()
        {
            List<Model_CheckList4PM> cl = new List<Model_CheckList4PM>();
            var obj = DataAccessLayer._instance.CheckListDGCDAQSAE(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new Model_CheckList4PM()
                {
                    CheckListID = int.Parse(dRow["CheckListHeaderID"].ToString()),
                    Timestamp = DateTime.Parse(dRow["Timestamp"].ToString()),
                    CheckListData = DateTime.Parse(dRow["CheckData"].ToString()),
                    CheckedBy = dRow["CheckedBy"].ToString(),
                    Wbs = dRow["Wbs"].ToString(),
                    WbsDescription = dRow["WbsDescription"].ToString(),
                    ProjectManager = dRow["ProjectManager"].ToString(),
                    Site = dRow["Site"].ToString(),
                    Sector = dRow["Sector"].ToString(),
                    Subcontractor = dRow["Subcontractor"].ToString()
                });
            }
            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Model_CheckList4PM> GetAllBooks()
        {
            IList<Model_CheckList4PM> x = CheckListDGCDAQSAE();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<Model_CheckList4PM> x = CheckListDGCDAQSAE();
            var books = x.FirstOrDefault((p) => p.CheckListID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIAree : Controller
    {
        public IList<CplArea> cplarea()
        {
            List<CplArea> cl = new List<CplArea>();
            var obj = DataAccessLayer._instance.CplAree(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new CplArea()
                {
                    AreaID = int.Parse(dRow["CplAreaID"].ToString()),
                    Area = dRow["CplArea"].ToString()
                });
            }
            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<CplArea> GetAllBooks()
        {
            IList<CplArea> x = cplarea();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<CplArea> x = cplarea();
            var books = x.FirstOrDefault((p) => p.AreaID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIAreeAll : Controller
    {
        public IList<CplArea> cplarea()
        {
            List<CplArea> cl = new List<CplArea>();
            var obj = DataAccessLayer._instance.CplAreeAll(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new CplArea()
                {
                    AreaID = int.Parse(dRow["CplAreaID"].ToString()),
                    Area = dRow["CplArea"].ToString()
                });
            }
            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<CplArea> GetAllBooks()
        {
            IList<CplArea> x = cplarea();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<CplArea> x = cplarea();
            var books = x.FirstOrDefault((p) => p.AreaID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class ChkAPIHeadAll : Controller
    {
        public IList<CplHead> cplhead()
        {
            List<CplHead> cl = new List<CplHead>();
            var obj = DataAccessLayer._instance.CplHeadAll(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new CplHead()
                {
                    HeadID = int.Parse(dRow["CplHeadID"].ToString()),
                    Head = dRow["CplHead"].ToString()
                });
            }
            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<CplHead> GetAllBooks()
        {
            IList<CplHead> x = cplhead();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<CplHead> x = cplhead();
            var books = x.FirstOrDefault((p) => p.HeadID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    [Route("api/[controller]")]
    public class FileManagerApi : Controller
    {
        public IList<CplHead> cplhead()
        {
            List<CplHead> cl = new List<CplHead>();
            var obj = DataAccessLayer._instance.CplHeadAll(User.Identity.Name);

            foreach (DataRow dRow in obj.Result.ds.Tables[0].Rows)
            {
                cl.Add(new CplHead()
                {
                    HeadID = int.Parse(dRow["CplHeadID"].ToString()),
                    Head = dRow["CplHead"].ToString()
                });
            }
            return cl;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<CplHead> GetAllBooks()
        {
            IList<CplHead> x = cplhead();
            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IList<CplHead> x = cplhead();
            var books = x.FirstOrDefault((p) => p.HeadID == id);

            var item = books;
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        //POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            HttpResponseMessage x = null;
            var respM = x;
            var j = JsonConvert.DeserializeObject<WorkOrderClosing>(value.ToString());
            //j.ReportName = "reportMitMan";
            //j.WOCode = "10950100839601";
            //j.WOID = "411891";
            //j.SnDevice = "MIT0030012698372";
            //j.sendmit = true;
            //j.NetworkId = 12341;
            try
            {
                var sys = await DataAccessLayer._instance.SysConfigMap();
                var mail = new Mail();
                DirectoryInfo parent = null;
                DirectoryInfo child = null;

                
                string path = sys.CertificatePathToElaborate;
                string subpath = j.SnDevice + "_" + j.WOID;
                
                parent = Directory.CreateDirectory(path);
                child = parent.CreateSubdirectory(subpath);
                var report = sys.ReportServer_URL;
                string sTargetURL = report + "/Pages/ReportViewer.aspx?%2frsEmployee%2f" + j.ReportName + "&ID_MAN=" + j.WOID + "&rs:Format=PDF";
                string MitName = "MIT_" + j.WOCode + ".pdf";

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sTargetURL);
         
                req.Credentials = CredentialCache.DefaultCredentials;// new System.Net..NetworkCredential(
        
                HttpWebResponse HttpWResp = (HttpWebResponse)req.GetResponse();
                Stream fStream = HttpWResp.GetResponseStream();

                //string sTargetRDL = report + "/Pages/ReportViewer.aspx?%2fEnrollmentProd%2fRDL&IdOdl=" + j.WOID + "&rs:Format=PDF";
                //string RDLName = "RDL_" + j.WOCode + ".pdf";
                //HttpWebRequest reqR = (HttpWebRequest)WebRequest.Create(sTargetURL);

                //reqR.Credentials = CredentialCache.DefaultCredentials;// new System.Net..NetworkCredential(

                //HttpWebResponse HttpRResp = (HttpWebResponse)req.GetResponse();
                //Stream RStream = HttpRResp.GetResponseStream();

                //Now turn around and send this as the response..

                // Could save to a database or file here as well.
                //List<string> doc = new List<string>();
                //doc.Add(MitName);
                //doc.Add(RDLName);
                //foreach (var item in doc)
                //{
                using (FileStream MyFileStream = new FileStream(path + subpath + "\\" + MitName, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
                {
                    long FileSize;
                    FileSize = MyFileStream.Length;
                    byte[] Buffer = Business.Business._instance.ReadFully(fStream);
                    await MyFileStream.WriteAsync(Buffer, 0, Buffer.Length - 1);

                    mail.Subject = "Modulo Intervento Tecnico PDR: " + j.WOCode + " ODL: " + j.WOID;
                    mail.AttachmentName.Add(MitName);
                    mail.Recipient = /*"lspinelli@cpl.it";*//* TODO : email ricevente da db */ DataAccessLayer._instance.GetRecipient(j.NetworkId).Result.Recipient;
                    mail.DisplaySender = "Metering Department - CPL Concordia";
                    mail.Sender = DataAccessLayer._instance.SysConfigMap().Result.MailNotificationSender;
                    mail.stream.Add(Buffer);
                    mail.Message = "Buongiorno, in allegato la documentazione per l'intervento sull'impianto PDR: " + j.WOCode + "<br><br>Attenzione questo è un messaggio ad invio automatico, non rispondere a questo indirizzo, per dubbi o perplessità scrivere a: supportmetering@cpl.it";
                    if (j.sendmit)
                    {
                    /* TODO : generale CE_CVF */
                    // aggiungere allegato CE se ecor4
                    // odl = WOID
                    // pdr = WOCode
                    // select * from [tbl_vc] where ID_MOD_VC = 92 and SN_ECOR = 'j.SnDevice'
                    // se esce qualcosa
                    // "\\sr-concordia\elabo$\" + "Elabo\"+ anno +"_Elaborati\*" & j.SnDevice & "*.pdf"
                    // loop su anno: da 2016 a anno attuale
                    // wildcard per nome pdf System.IO.Directory.GetFiles
                    // Directory.EnumerateFiles(String, String, SearchOption.AllDirectories)

                        if ((j.Model == "ECOR4") && (j.Sost == "SI" || j.Wot == "Installazione"))
                        {
                            try
                            {
                                var ceFiles = Directory.EnumerateFiles(@"\\sr-concordia\elabo$\", "*" + j.SnDevice + "*.pdf", SearchOption.AllDirectories);

                                foreach (string currentFile in ceFiles)
                                {
                                    using (FileStream ceStream = new FileStream(currentFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                                    {
                                        byte[] buff = Business.Business._instance.ReadFully(ceStream);
                                        mail.stream.Add(buff);
                                        mail.AttachmentName.Add("CE_CVF_" + j.SnDevice + ".pdf");
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = e.ToString() };
                            }
                        }
                        var sendObj = await Business.Business._instance.SendEmail(mail);
                        if (sendObj.success)
                        {
                            resp = new HttpResponseMessage(HttpStatusCode.OK);
                        }
                        else
                        {
                            resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = sendObj.response };
                        }
                    }

                }

                //}
                HttpWResp.Close();

                //mail object declaration

                /* TODO : TEMP decommentare chiusura odl a db*/
                //resp = new HttpResponseMessage(HttpStatusCode.OK);
                var close = await DataAccessLayer._instance._CloseWorkOrder(j, User.Identity.Name);
                if (close.success)
                {
                    resp = new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = close.response };
                }

            }
            catch (Exception e)
            {

                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = e.ToString() };
            }
            return resp;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    #endregion

    #region EAIWEB_APIS

    [Route("api/[controller]")]
    public class WebApiGetSocietà : Controller
    {
        public async Task<IList<Società>> GetAllSocietà()
        {
            var obj = await DataAccessLayer._instance.SocietàList();
            return obj;
        }
    }

    [Route("api/[controller]")]
    public class WebApiGetFlussi : Controller
    {
        //Funzione richiamata da tutti i diversi WS legati ai flussi
        public async Task<IList<Flusso>> FlussoList()
        {
            var obj = await DataAccessLayer._instance.FlussoList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<Flusso>> GetAllFlusso()
        {
            IList<Flusso> x = await DataAccessLayer._instance.FlussoList();

            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<Flusso> x = await FlussoList();

            var flusso = x.Where((p) => p.idflusso == id);

            var item = flusso;

            if (item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]JToken value)
        {
            HttpResponseMessage y = null;
            var resp = y;
            var j = JsonConvert.DeserializeObject<WorkOrderClosing>(value.ToString());
            var obj = await DataAccessLayer._instance._CloseWorkOrder(j, User.Identity.Name);
            if (obj.success == true)
            {
                resp = new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = obj.response };
            }
            return resp;
        }

    }

    [Route("api/[controller]")]
    public class WebApiGetFlussiAttiviStampatore : Controller
    {
        //Funzione richiamata da tutti i diversi WS legati ai flussi
        public async Task<IList<Flusso>> FlussiAttiviStampatoreList()
        {
            var obj = await DataAccessLayer._instance.FlussiAttiviStampatoreList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<Flusso>> GetAllFlussiAttiviStampatore()
        {
            IList<Flusso> x = await DataAccessLayer._instance.FlussiAttiviStampatoreList();

            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<Flusso> x = await FlussiAttiviStampatoreList();

            var flusso = x.Where((p) => p.idflusso == id);

            var item = flusso;

            if (item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

    }

    [Route("api/[controller]")]
    public class WebApiGetFlussiTerminatiStampatore : Controller
    {
        //Funzione richiamata da tutti i diversi WS legati ai flussi
        public async Task<IList<Flusso>> FlussiTerminatiStampatoreList()
        {
            var obj = await DataAccessLayer._instance.FlussiAttiviStampatoreList();
            return obj;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<Flusso>> GetAllFlussiTerminatiStampatore()
        {
            IList<Flusso> x = await DataAccessLayer._instance.FlussiTerminatiStampatoreList();

            return x;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            IList<Flusso> x = await FlussiTerminatiStampatoreList();

            var flusso = x.Where((p) => p.idflusso == id);

            var item = flusso;

            if (item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

    }

    #endregion

}