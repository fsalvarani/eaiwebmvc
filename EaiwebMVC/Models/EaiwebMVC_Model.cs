﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EaiwebMVC.Models
{

    #region EAIWEB_Declares

    public class Società
    {
        public string id_soc { get; set; }
        public string descrizione { get; set; }
        public string nometxt { get; set; }
        public string societa { get; set; }
        public string codsoc { get; set; }
        public string indirizzo_corriere { get; set; }
        public string email_billing { get; set; }
        public string testo_mail { get; set; }
        public string mail_societa { get; set; }
        public string cartella_pdf { get; set; }
        public string programma_fatt { get; set; }
        public string server_db { get; set; }
        public string user_db { get; set; }
        public string pwd_db { get; set; }
        public string database_db { get; set; }
        public string tabella_db { get; set; }
        public string tipo_archivio { get; set; }
        public string query_elenco_fatture { get; set; }
        public string email_billing_ee { get; set; }
        public string testo_mail_solleciti { get; set; }
        public string mail_societa_solleciti { get; set; }
        public string IDIsolutions { get; set; }
        public string FormatoNumeroFattura { get; set; }
        public bool? gestione_SDI { get; set; }
        public string id_manager { get; set; }
        public string stampatore_bollette { get; set; }
        public string stampatore_solleciti { get; set; }
        public string stampatore_welcome { get; set; }
    }

    public class Flusso
    {
        public string       idflusso { get; set; }
        public DateTime?    datainizio { get; set; }
        public DateTime?    datafine { get; set; }
        public string       step { get; set; }
        public int          totpagine { get; set; }
        public int          totfatture { get; set; }
        public int          numfile { get; set; }
        public int          bollettini { get; set; }
        public int          pagineaggiuntive { get; set; }
        public int          allegati { get; set; }
        public string       confermafatt { get; set; }
        public string       confermastamp { get; set; }
        public string       mailconfermafatt { get; set; }
        public string       consegna { get; set; }
        public string       stampatore { get; set; }
        public string       allegato_interno { get; set; }
        public string       allegato_esterno { get; set; }
        public string       doc { get; set; }
        public string       dataora { get; set; }
        public string       note { get; set; }
        public string       filetxt { get; set; }
        public string       mailftp { get; set; }
        public string       data_emissione { get; set; }
        public string       pesi { get; set; }
        public string       colore { get; set; }
        public string       pesi_posta { get; set; }
        public string       pesi_tnt { get; set; }
        public string       pesi_fulmine { get; set; }
        public string         validazione_num_doc { get; set; }

    }

    #endregion
    
    public class WorkOrderPlanningModel
    {
        public string Pdr { get; set; }
        public string Latitudine { get; set; }
        public string Longitudine { get; set; }
        public string Remi { get; set; }
        public string CustomerID { get; set; }
        public string Customer { get; set; }
        public string Network { get; set; }
        public string Plant { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Prov { get; set; }
        public DateTime? PlanningDate { get; set; }
        public string Technician { get; set; }
        public string WorkOrdeType { get; set; }
        public string Nota { get; set; }
        public int? WorkOrder { get; set; }
        public int? WorkOrderChild { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public string UcStop { get; set; }
        public string VmStop { get; set; }
        public string VbStop { get; set; }
        public string UcRestart { get; set; }
        public string VmRestart { get; set; }
        public string VbRestart { get; set; }
        public string Wbs { get; set; }
        public string ICAgreement { get; set; }
        public string MeterSn { get; set; }
        public string MeterBrand { get; set; }
        public string DeviceSn { get; set; }
        public string DeviceBrand { get; set; }
        public string DeviceModel { get; set; }
        public string Activity { get; set; }
        //public string Ispettore { get; set; }
        //public bool isSuccess { get; set; }
    }
    public class WorkOrderBalanceModel
    {
        public string Pdr { get; set; }
        public string Latitudine { get; set; }
        public string Longitudine { get; set; }
        public string Remi { get; set; }
        public string CustomerID { get; set; }
        public string Customer { get; set; }
        public string Network { get; set; }
        public string Plant { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Prov { get; set; }
        public DateTime? PlanningDate { get; set; }
        public string Technician { get; set; }
        public string WorkOrdeType { get; set; }
        public string Nota { get; set; }
        public int? WorkOrder { get; set; }
        public int? WorkOrderChild { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public string UcStop { get; set; }
        public string VmStop { get; set; }
        public string VbStop { get; set; }
        public string UcRestart { get; set; }
        public string VmRestart { get; set; }
        public string VbRestart { get; set; }
        public string Wbs { get; set; }
        public string ICAgreement { get; set; }
        public string MeterSn { get; set; }
        public string MeterBrand { get; set; }
        public string DeviceSn { get; set; }
        public string DeviceBrand { get; set; }
        public string DeviceModel { get; set; }
        public string Activity { get; set; }
        //public string Ispettore { get; set; }
        //public bool isSuccess { get; set; }
    }
    public class RemiModel
    {
        public string code { get; set; }
        public string cap { get; set; }
        public string co2 { get; set; }
        public string h2 { get; set; }
        public string n2 { get; set; }
        public string zs { get; set; }
        public string rho15 { get; set; }
        public string d15 { get; set; }
        public string pcs15 { get; set; }
        public string rho0 { get; set; }
        public string d0 { get; set; }
        public string pcs0 { get; set; }
        public string validityPeriod { get; set; }
        public string lastUpdate { get; set; }
        public string nextUpdate { get; set; }
        public string enableUpdate { get; set; }
        public string holder { get; set; }
        public string esercizio { get; set; }
        public string assoluto { get; set; }
        public string comune { get; set; }
        public string provincia { get; set; }
    }
    public class Model_CheckList4PM
    {
        public int CheckListID { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime CheckListData { get; set; }
        public string CheckedBy { get; set; }
        public string Wbs { get; set; }
        public string WbsDescription { get; set; }
        public string ProjectManager { get; set; }
        public string Site { get; set; }
        public string Sector { get; set; }
        public string Subcontractor { get; set; }
    }
    public class dataToService
    {
        public bool isSuccess { get; set; }
        public string response { get; set; }
        public DataSet ds { get; set; }
    }
    public class UserRole
    {
        public bool isCC { get; set; }
        public bool isDPS { get; set; }
        public bool isRSA { get; set; }
        public bool isDGCDAQSAE { get; set; }
        public bool isDenied { get; set; }
    }
    public class user
    {
        public string username { get; set; }
        public string sector { get; set; }
        public string uid { get; set; }
        public bool isSuccess { get; set; }
        public string response { get; set; }
    }
    public class CplArea
    {
        public int AreaID { get; set; }
        public string Area { get; set; }
    }
    public class CplHead
    {
        public int HeadID { get; set; }
        public string Head { get; set; }
    }
    public class ICModel
    {
        public string IC { get; set; }
        public string ICDescription { get; set; }
        public int CustomerID { get; set; }
        public string Customer { get; set; }
        public string Wbs { get; set; }
        public string WbsDescription { get; set; }
        public bool isSuccess { get; set; }
        public string response { get; set; }
    }
    public class PlantModel
    {
        public string ElementCode { get; set; }
        public string ElementName { get; set; }
        public int CustomerID { get; set; }
        public string CustomerDescription { get; set; }
        public string ElementAddress { get; set; }
        public string ElementStreetNumber { get; set; }
        public string ElementZipCode { get; set; }
        public string ElementCity { get; set; }
        public string ElementPrv { get; set; }
        public string ElementRegion { get; set; }
        public string ElementState { get; set; }
        public string ElementLat { get; set; }
        public string ElementLng { get; set; }
        public string Sn { get; set; }
        public string SnCont { get; set; }
        public string ModConv { get; set; }
        public string ModCont { get; set; }
        public string Stato { get; set; }
        public string MarcaConv { get; set; }
        public string MarcaCont { get; set; }
        public string Sim { get; set; }
        public string IdDeviceTypology { get; set; }
        public string Remi { get; set; }
        public string IdSr { get; set; }
        public string DescSr { get; set; }
    }
    public class CustomerModel
    {
        public int CustomerID { get; set; }
        public string CustomerDescription { get; set; }
        public string PartitaIva { get; set; }
    }
    public class AppNews
    {
        public string CLCount { get; set; }
        public bool isSuccess { get; set; }
    }
    public class WorkOrderModel
    {
        public string WOPK { get; set; }
        public string WOID { get; set; }
        public string WOKey { get; set; }
        public string WOElement { get; set; }
        public int WOTypeID { get; set; }
        public string WOType { get; set; }
        public int? WOState { get; set; }
        public int WODID { get; set; }
        public int WOCID { get; set; }
        public string WOStateDescription { get; set; }
        public string WOCustomer { get; set; }
        public string CreatedBy { get; set; }
        public string CancelledBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? WorkmanID { get; set; }
        public string AssignedTo { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public DateTime? ScheduledDateEnd { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public int? ReasonCode { get; set; }
        public string ReasonText { get; set; }
        public string wbs { get; set; }
        public string ic { get; set; }
        public string CustomerOrder { get; set; }
        public string Activity { get; set; }
        public string Esito { get; set; }
        public string IsActive { get; set; }
        public bool AllDay { get; set; } = true; 
    }
    public class WorkOrderPDFModel
    {
        public string WOID { get; set; }
        public string WOCode { get; set; }
        public string SnDevice { get; set; }
        public string ReportName { get; set; }
    }
    public class WorkOrderDetailsModel
    {
        public int WOID { get; set; }
        public string WorkOrderDetails { get; set; }
        public string MaterialsCode { get; set; }
        public string MaterialsDescription { get; set; }
        public int? Qty { get; set; }
        public string Price { get; set; }
    }
    public class WorkOrderInvoiceModel
    {
        public string WOID { get; set; }
        public string WOElement { get; set; }
        public int WOTID { get; set; }
        public string WOType { get; set; }
        public int WOSID { get; set; }
        public string WOStateDescription { get; set; }
        public int WOCID { get; set; }
        public string WOCustomer { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? WorkmanID { get; set; }
        public string AssignedTo { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public DateTime? ScheduledDateEnd { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public int? ReasonCode { get; set; }
        public string ReasonText { get; set; }
        public string wbs { get; set; }
        public string ic { get; set; }
        public string CustomerOrder { get; set; }
        public float? HoursTravelling { get; set; }
        public float? HoursWorkman { get; set; }
    }
    public class WorkmanModel
    {
        public int WorkmanID { get; set; }
        public string WorkmanName { get; set; }
    }
    public class WorkOrderTypeModel
    {
        public int WorkOrderTypeID { get; set; }
        public string WorkOrderType { get; set; }
    }
    public class WorkOrderStatusModel
    {
        public int WorkOrderStatusID { get; set; }
        public string WorkOrderStatus { get; set; }
    }
    public class SectorModel
    {
        public string SectorTypeId { get; set; }
        public string SectorType { get; set; }
    }
    public class WbsrModel
    {
        public int CustomerId { get; set; }
        public string Wbs { get; set; }
        public string WbsDescription { get; set; }
    }
    public class PriceListrModel
    {
        public int PriceListId { get; set; }
        public int CustomerId { get; set; }
        public string Item { get; set; }
        public string ItemPrice { get; set; }
        public string Wbs { get; set; }
    }
    public class WorkOrderWFMModel
    {
        public int Odl { get; set; }
        public string WorkOrderType { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public string Pdr { get; set; }
        public string SnMeter { get; set; }
        public string BrandMeter { get; set; }
        public string SnDevice { get; set; }
        public string DeviceModel { get; set; }
        public string Fw { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Network { get; set; }
        public int NetworkId { get; set; }
        public string PlantName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string ZipCode { get; set; }
        public string UcStop { get; set; }
        public string VmStop { get; set; }
        public string VbStop { get; set; }
        public string UcRestart { get; set; }
        public string VmRestart { get; set; }
        public string VbRestart { get; set; }
        public string Note { get; set; }
        public string WorkerMan { get; set; }
        public string Stato { get; set; }
        public string Sostituzione { get; set; }
        public string ReportName { get; set; }
        public string isThereVerification { get; set; }
        public string VerificationResult { get; set; }
        public bool sendmit { get; set; }
        public int TypeOdlId { get; set; }            
    }
    public class WorkOrderImgWFMModel
    {
        public int Odl { get; set; }
        public string Pdr { get; set; }
        public string Name { get; set; }
        public string Img { get; set; }
        public string Path { get; set; }
        public string VbStop { get; set; }
        public string VmStop { get; set; }
        public string UCStop { get; set; }
        public string VbRestart { get; set; }
        public string VmRestart { get; set; }
        public string UCRestart { get; set; }
        public string PhotoType { get; set; }
    }
    public class WorkOrderClosing
    {
        public string WOID { get; set; }
        public string WOCode { get; set; }
        public int CustomerId { get; set; }
        public int NetworkId { get; set; }
        public string SnDevice { get; set; }
        public string ReportName { get; set; }
        public bool sendmit { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string Model { get; set; }
        public string Sost { get; set; }
        public string Wot { get; set; }
    }
    public class WorkOrderDetails
    {
        public int woid { get; set; }
        public DateTime Timestamp { get; set; }
        public string Activity { get; set; }
        public string Solution { get; set; }
    }
    public class Mail
    {
        public string Subject { get; set; }
        public string DisplaySender { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string Message { get; set; }
        public List<string> AttachmentName { get; set; }
        public List<byte[]> stream { get; set; }

        public Mail() {
            AttachmentName = new List<string>();
            stream = new List<byte[]>();
        }
    }
    public class SysConfig
    {
        public string CertificatePathElaborate { get; set; }
        public string CertificatePathToElaborate { get; set; }
        public string PROSPECT_WS_URL { get; set; }
        public string ReportServer_URL { get; set; }
        public string MailNotificationSender { get; set; }
    }
    public class NgActivity
    {
        public int Odl { get; set; }
        public DateTime ActivityDate { get; set; }
        public string PlantCode { get; set; }
        public string Customer { get; set; }
        public string Plant { get; set; }
        public string PlantAddress { get; set; }
        public string LAT { get; set; }
        public string LONG { get; set; }
        public string TypeOdl { get; set; }
        public string Technician { get; set; }
    }
    public class NgActivityStat
    {
        public int ActivitiesCount { get; set; }
        public string ActivitiesDescription { get; set; }
        public int ActivitiesAvg { get; set; }
    }
    public class DataResponse
    {
        public string response { get; set; }
        public bool success { get; set; }
        public DataSet data { get; set; }
    }
}