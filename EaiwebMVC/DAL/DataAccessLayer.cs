﻿using EaiwebMVC.Models;
using log4net;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EaiwebMVC.DAL
{
    public static class ReaderExtensions
    {
        public static DateTime? GetNullableDateTime(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);

            return reader.IsDBNull(col) ?
                        null :
                        (DateTime?)reader.GetDateTime(col);
        }

        public static int? GetNullableInt(this SqlDataReader reader, string name)
        {
            var col = reader.GetOrdinal(name);

            return reader.IsDBNull(col) ?
                        null :
                        (int?)reader.GetInt32(col);
        }
    }

    public class DataAccessLayer
    {
        #region "ConnectionString"
        public SqlConnectionStringBuilder cString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "wfmdb";   //  "NB-BORTOLIST\\SQLEXPRESS01";//"wfmdb";   //  
            builder.UserID = "saenrollment";              //  update me
            builder.Password = "enrollment";      // update me
            builder.InitialCatalog = "employeedb";
            return builder;
        }

        public SqlConnectionStringBuilder EnrollmentString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "wfmdb";   //  "NB-BORTOLIST\\SQLEXPRESS01";//"wfmdb";   //  
            builder.UserID = "saenrollment";              //  update me
            builder.Password = "enrollment";      // update me
            builder.InitialCatalog = "EnrollmentDB";
            return builder;
        }

        public SqlConnectionStringBuilder cStringEaiweb()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "SR-DBT1.CPGNET.LOCAL";   //  "NB-BORTOLIST\\SQLEXPRESS01";//"wfmdb";   //  
            builder.UserID = "sabem";              //  update me
            builder.Password = "bemsa";      // update me
            builder.InitialCatalog = "TEST_EAIWEB";
            return builder;
        }

        #endregion

        #region "Log"

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        public static readonly DataAccessLayer _instance = new DataAccessLayer();

        #region Select_Eaiweb

        public async Task<IList<Società>> SocietàList()
        {
            //FS_Dichiaro una lista di elementi di tipo "Società"
            List<Società> list = new List<Società>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cStringEaiweb().ConnectionString))
                {
                    await cn1.OpenAsync();


                    using (SqlCommand command = new SqlCommand("mvc_select_societa_list"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 30000;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            //Creo un'istanza di società
                            Società obj = new Società();

                            //Vi assegno i valori prelevati dalla query
                            obj.id_soc =                 reader["id_soc"].ToString();
                            obj.descrizione =            reader["descrizione"].ToString();
                            obj.nometxt =                reader["nometxt"].ToString();
                            obj.societa =                reader["societa"].ToString();
                            obj.codsoc =                 reader["codsoc"].ToString();
                            obj.indirizzo_corriere =     reader["indirizzo_corriere"].ToString();
                            obj.email_billing =          reader["email_billing"].ToString();
                            obj.testo_mail =             reader["testo_mail"].ToString();
                            obj.mail_societa =           reader["mail_societa"].ToString();
                            obj.cartella_pdf =           reader["cartella_pdf"].ToString();
                            obj.programma_fatt =         reader["programma_fatt"].ToString();
                            obj.server_db =              reader["server_db"].ToString();
                            obj.user_db =                reader["user_db"].ToString();
                            obj.pwd_db =                 reader["pwd_db"].ToString();
                            obj.database_db =            reader["database_db"].ToString();
                            obj.tabella_db =             reader["tabella_db"].ToString();
                            obj.tipo_archivio =          reader["tipo_archivio"].ToString();
                            obj.query_elenco_fatture =   reader["query_elenco_fatture"].ToString();
                            obj.email_billing_ee =       reader["email_billing_ee"].ToString();
                            obj.testo_mail_solleciti =   reader["testo_mail_solleciti"].ToString();
                            obj.mail_societa_solleciti = reader["mail_societa_solleciti"].ToString();
                            obj.IDIsolutions =           reader["IDIsolutions"].ToString();
                            obj.FormatoNumeroFattura =   reader["FormatoNumeroFattura"].ToString();   
                            obj.gestione_SDI =           (bool)reader["gestione_SDI"];
                            obj.id_manager =             reader["id_manager"].ToString();
                            obj.stampatore_bollette =    reader["stampatore_bollette"].ToString();
                            obj.stampatore_solleciti =   reader["stampatore_solleciti"].ToString();
                            obj.stampatore_welcome =     reader["stampatore_welcome"].ToString();
                            
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<Flusso>> FlussoList()
        {
            //FS_Dichiaro una lista di elementi di tipo "Flusso"
            List<Flusso> list = new List<Flusso>();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cStringEaiweb().ConnectionString))
                {
                    await cn1.OpenAsync();


                    using (SqlCommand command = new SqlCommand("mvc_select_flusso_list"))
                    {
                        command.Connection = cn1;
                        command.CommandTimeout = 30000;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            //Creo un'istanza di flusso
                            Flusso obj = new Flusso();

                            obj.idflusso = reader["idflusso"].ToString();
                           
                            obj.datainizio = reader.GetNullableDateTime("datainizio");
                              //  (DateTime?)(reader.IsDBNull(colNumber) ? null:reader["datainizio"]);
                            obj.datafine = reader.GetNullableDateTime("datafine");
                            obj.step = reader["step"].ToString();
                            obj.totpagine = (int)reader["totpagine"];
                            obj.totfatture = (int)reader["totfatture"];
                            obj.numfile = (int)reader["numfile"];
                            obj.bollettini = (int)reader["bollettini"];
                            obj.pagineaggiuntive = (int)reader["pagineaggiuntive"];
                            obj.allegati = (int)reader["allegati"];
                            obj.confermafatt = reader["confermafatt"].ToString();
                            obj.confermastamp = reader["confermastamp"].ToString();
                            obj.mailconfermafatt = reader["mailconfermafatt"].ToString();
                            obj.consegna = reader["consegna"].ToString();
                            obj.stampatore = reader["stampatore"].ToString();
                            obj.allegato_interno = reader["allegato_interno"].ToString();
                            obj.allegato_esterno = reader["allegato_esterno"].ToString();
                            obj.doc = reader["doc"].ToString();
                            obj.dataora = reader["dataora"].ToString();
                            obj.note = reader["note"].ToString();
                            obj.filetxt = reader["filetxt"].ToString();
                            obj.mailftp = reader["mailftp"].ToString();
                            obj.data_emissione = reader["data_emissione"].ToString();
                            obj.pesi = reader["pesi"].ToString();
                            obj.colore = reader["colore"].ToString();
                            obj.pesi_posta = reader["pesi_posta"].ToString();
                            obj.pesi_tnt = reader["pesi_tnt"].ToString();
                            obj.pesi_fulmine = reader["pesi_fulmine"].ToString();

                            obj.validazione_num_doc = reader["validazione_num_doc"].ToString();

                            list.Add(obj);
                        }
                    }
                    //test
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();

                log.Error(ex.Message + " | " + line.ToString(), ex);
            }

            return list;
        }

        public async Task<IList<Flusso>> FlussiAttiviStampatoreList()
        {
            //FS_Dichiaro una lista di elementi di tipo "Flusso"
            List<Flusso> list = new List<Flusso>();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cStringEaiweb().ConnectionString))
                {
                    await cn1.OpenAsync();


                    using (SqlCommand command = new SqlCommand("mvc_select_flussi_stampatore_attivi"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 3000;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            //Creo un'istanza di flusso
                            Flusso obj = new Flusso();

                            obj.idflusso = reader["idflusso"].ToString();

                            obj.datainizio = reader.GetNullableDateTime("datainizio");
                            obj.datafine = reader.GetNullableDateTime("datafine");
                            obj.step = reader["step"].ToString();
                            obj.totpagine = (int)reader["totpagine"];
                            obj.totfatture = (int)reader["totfatture"];
                            obj.numfile = (int)reader["numfile"];
                            obj.bollettini = (int)reader["bollettini"];
                            obj.pagineaggiuntive = (int)reader["pagineaggiuntive"];
                            obj.allegati = (int)reader["allegati"];
                            obj.confermafatt = reader["confermafatt"].ToString();
                            obj.confermastamp = reader["confermastamp"].ToString();
                            obj.mailconfermafatt = reader["mailconfermafatt"].ToString();
                            obj.consegna = reader["consegna"].ToString();
                            obj.stampatore = reader["stampatore"].ToString();
                            obj.allegato_interno = reader["allegato_interno"].ToString();
                            obj.allegato_esterno = reader["allegato_esterno"].ToString();
                            obj.doc = reader["doc"].ToString();
                            obj.dataora = reader["dataora"].ToString();
                            obj.note = reader["note"].ToString();
                            obj.filetxt = reader["filetxt"].ToString();
                            obj.mailftp = reader["mailftp"].ToString();
                            obj.data_emissione = reader["data_emissione"].ToString();
                            obj.pesi = reader["pesi"].ToString();
                            obj.colore = reader["colore"].ToString();
                            obj.pesi_posta = reader["pesi_posta"].ToString();
                            obj.pesi_tnt = reader["pesi_tnt"].ToString();
                            obj.pesi_fulmine = reader["pesi_fulmine"].ToString();
                            obj.validazione_num_doc = reader["validazione_num_doc"].ToString();

                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<Flusso>> FlussiTerminatiStampatoreList()
        {
            //FS_Dichiaro una lista di elementi di tipo "Flusso"
            List<Flusso> list = new List<Flusso>();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cStringEaiweb().ConnectionString))
                {
                    await cn1.OpenAsync();

                    using (SqlCommand command = new SqlCommand("mvc_select_flussi_stampatore_terminati"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 3000;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            //Creo un'istanza di flusso
                            Flusso obj = new Flusso
                            {
                                idflusso = reader["idflusso"].ToString(),

                                datainizio = reader.GetNullableDateTime("datainizio"),
                                //  (DateTime?)(reader.IsDBNull(colNumber) ? null:reader["datainizio"]);
                                datafine = reader.GetNullableDateTime("datafine"),
                                step = reader["step"].ToString(),
                                totpagine = (int)reader["totpagine"],
                                totfatture = (int)reader["totfatture"],
                                numfile = (int)reader["numfile"],
                                bollettini = (int)reader["bollettini"],
                                pagineaggiuntive = (int)reader["pagineaggiuntive"],
                                allegati = (int)reader["allegati"],
                                confermafatt = reader["confermafatt"].ToString(),
                                confermastamp = reader["confermastamp"].ToString(),
                                mailconfermafatt = reader["mailconfermafatt"].ToString(),
                                consegna = reader["consegna"].ToString(),
                                stampatore = reader["stampatore"].ToString(),
                                allegato_interno = reader["allegato_interno"].ToString(),
                                allegato_esterno = reader["allegato_esterno"].ToString(),
                                doc = reader["doc"].ToString(),
                                dataora = reader["dataora"].ToString(),
                                note = reader["note"].ToString(),
                                filetxt = reader["filetxt"].ToString(),
                                mailftp = reader["mailftp"].ToString(),
                                data_emissione = reader["data_emissione"].ToString(),
                                pesi = reader["pesi"].ToString(),
                                colore = reader["colore"].ToString(),
                                pesi_posta = reader["pesi_posta"].ToString(),
                                pesi_tnt = reader["pesi_tnt"].ToString(),
                                pesi_fulmine = reader["pesi_fulmine"].ToString(),

                                validazione_num_doc = reader["validazione_num_doc"].ToString()
                            };

                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        #endregion

        #region Insert_Eaiweb



        #endregion

        #region "Select"

        public async Task<IList<RemiModel>> RemiList()
        {
            List<RemiModel> list = new List<RemiModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetRemiList"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            RemiModel obj = new RemiModel();
                            obj.code = reader["Code"].ToString();
                            obj.cap = reader["CAP"].ToString();
                            obj.co2 = reader["CO2"].ToString();
                            obj.h2 = reader["H2"].ToString();
                            obj.n2 = reader["N2"].ToString();
                            obj.zs = reader["Zs"].ToString();
                            obj.rho15 = reader["Rho15"].ToString();
                            obj.d15 = reader["d15"].ToString();
                            obj.pcs15 = reader["PCS15"].ToString();
                            obj.rho0 = reader["Rho0"].ToString();
                            obj.d0 = reader["d0"].ToString();
                            obj.pcs0 = reader["PCS0"].ToString();
                            obj.validityPeriod = reader["ValidityPeriod"].ToString();
                            obj.lastUpdate = reader["LastUpdate"].ToString();
                            obj.nextUpdate = reader["NextUpdate"].ToString();
                            obj.enableUpdate = reader["EnableUpdate"].ToString();
                            obj.holder = reader["Holder"].ToString();
                            obj.esercizio = reader["Esercizio"].ToString();
                            obj.assoluto = reader["Assoluto"].ToString();
                            obj.comune = reader["Comune"].ToString();
                            obj.provincia = reader["Provincia"].ToString();

                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<WorkOrderPlanningModel>> WorkOrderPlanning()
        {
            List<WorkOrderPlanningModel> list = new List<WorkOrderPlanningModel>();
            List<WorkOrderPlanningModel> OrderedList = new List<WorkOrderPlanningModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_queryCustomPlanning"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            //string executiondateinput = reader["Data Intervento"].ToString();

                            //if (string.IsNullOrEmpty(executiondateinput))
                            //{
                                WorkOrderPlanningModel obj = new WorkOrderPlanningModel
                                {
                                    Pdr = reader["Pdr"].ToString(),
                                    Latitudine = reader["Latitudine"].ToString(),
                                    Longitudine = reader["Longitudine"].ToString(),
                                    Remi = reader["Remi"].ToString(),
                                    CustomerID = reader["DistributoreID"].ToString(),
                                    Customer = reader["Distributore"].ToString(),
                                    Network = reader["Sottorete"].ToString(),
                                    Plant = reader["Utente"].ToString(),
                                    Address = reader["Indirizzo"].ToString(),
                                    City = reader["Città"].ToString(),
                                    ZipCode = reader["CAP"].ToString(),
                                    Prov = reader["Provincia"].ToString(),
                                    PlanningDate = reader.GetNullableDateTime("Data Programmata"),
                                    Technician = reader["Tecnico Assegnato"].ToString(),
                                    WorkOrdeType = reader["Tipo Odl"].ToString(),
                                    Nota = reader["Nota"].ToString(),
                                    WorkOrder = reader.GetNullableInt("Odl"),
                                    WorkOrderChild = reader.GetNullableInt("OdlChild"),
                                    //ExecutionDate = reader.GetNullableDateTime("Data Intervento"),
                                    UcStop = reader["UcStop"].ToString(),
                                    VmStop = reader["VmStop"].ToString(),
                                    VbStop = reader["VbStop"].ToString(),
                                    UcRestart = reader["UcRestart"].ToString(),
                                    VmRestart = reader["VmRestart"].ToString(),
                                    VbRestart = reader["VbRestart"].ToString(),
                                    Wbs = reader["Wbs"].ToString(),
                                    ICAgreement = reader["ICAgreement"].ToString(),
                                    MeterSn = reader["Matricola Contatore"].ToString(),
                                    MeterBrand = reader["Marca Contatore"].ToString(),
                                    DeviceSn = reader["Matricola Convertitore"].ToString(),
                                    DeviceBrand = reader["Marca convertitore"].ToString(),
                                    DeviceModel = reader["Modello Convertitore"].ToString(),
                                    Activity = reader["Attività"].ToString()
                                };
                                list.Add(obj);
                            //}
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
            OrderedList = list.OrderBy(x => x.Technician).ToList();
            return OrderedList;
        }

        public async Task<IList<WorkOrderBalanceModel>> WorkOrderCons()
        {
            List<WorkOrderBalanceModel> list = new List<WorkOrderBalanceModel>();
            List<WorkOrderBalanceModel> OrderedList = new List<WorkOrderBalanceModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_queryCustom"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            //string scheduleddateinput = reader["Data Programmata"].ToString();

                            string executiondateinput = reader["Data Intervento"].ToString();

                            if (executiondateinput != "")
                            {
                                WorkOrderBalanceModel obj = new WorkOrderBalanceModel
                                {
                                    Pdr = reader["Pdr"].ToString(),
                                    Customer = reader["Distributore"].ToString(),
                                    Network = reader["Sottorete"].ToString(),
                                    Plant = reader["Utente"].ToString(),
                                    Address = reader["Indirizzo"].ToString(),
                                    City = reader["Città"].ToString(),
                                    ZipCode = reader["CAP"].ToString(),
                                    Prov = reader["Provincia"].ToString(),
                                    Technician = reader["Tecnico Assegnato"].ToString(),
                                    WorkOrdeType = reader["Tipo Odl"].ToString(),
                                    WorkOrder = int.Parse(reader["Odl"].ToString()),
                                    Wbs = reader["Wbs"].ToString(),
                                    MeterSn = reader["Matricola Contatore"].ToString(),
                                    MeterBrand = reader["Marca Contatore"].ToString(),
                                    DeviceSn = reader["Matricola Convertitore"].ToString(),
                                    DeviceBrand = reader["Marca convertitore"].ToString(),
                                    DeviceModel = reader["Modello Convertitore"].ToString(),
                                    Activity = reader["Attività"].ToString()
                                };

                                if (DateTime.TryParse(executiondateinput, out DateTime MadeDate))
                                {
                                    obj.ExecutionDate = MadeDate;
                                }

                                list.Add(obj);
                            }
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
            OrderedList =
            /*return*/ list.OrderBy(x => x.Customer).ToList();
            return OrderedList;
        }

        public async Task<IList<WorkOrderBalanceModel>> WorkOrderCons(DateTime from, DateTime to)
        {
            List<WorkOrderBalanceModel> list = new List<WorkOrderBalanceModel>();
            List<WorkOrderBalanceModel> OrderedList = new List<WorkOrderBalanceModel>();
            try
            {
                var dFrom = from.Date;
                var dTo = to.Date;

                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_queryCustomBalanceTest"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@dataFrom", SqlDbType.Date).Value = from.Date;
                        command.Parameters.Add("@dataTo", SqlDbType.Date).Value = to.Date;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            //string scheduleddateinput = reader["Data Programmata"].ToString();

                            //string executiondateinput = reader["Data Intervento"].ToString();

                            //if (executiondateinput != "")
                            //{
                            WorkOrderBalanceModel obj = new WorkOrderBalanceModel
                            {
                                Pdr = reader["Pdr"].ToString(),
                                Latitudine = reader["Latitudine"].ToString(),
                                Longitudine = reader["Longitudine"].ToString(),
                                Remi = reader["Remi"].ToString(),
                                CustomerID = reader["DistributoreID"].ToString(),
                                Customer = reader["Distributore"].ToString(),
                                Network = reader["Sottorete"].ToString(),
                                Plant = reader["Utente"].ToString(),
                                Address = reader["Indirizzo"].ToString(),
                                City = reader["Città"].ToString(),
                                ZipCode = reader["CAP"].ToString(),
                                Prov = reader["Provincia"].ToString(),
                                PlanningDate = reader.GetNullableDateTime("Data Programmata"),
                                Technician = reader["Tecnico Assegnato"].ToString(),
                                WorkOrdeType = reader["Tipo Odl"].ToString(),
                                Nota = reader["Nota"].ToString(),
                                WorkOrder = reader.GetNullableInt("Odl"),
                                WorkOrderChild = reader.GetNullableInt("OdlChild"),
                                ExecutionDate = reader.GetNullableDateTime("Data Intervento"),
                                UcStop = reader["UcStop"].ToString(),
                                VmStop = reader["VmStop"].ToString(),
                                VbStop = reader["VbStop"].ToString(),
                                UcRestart = reader["UcRestart"].ToString(),
                                VmRestart = reader["VmRestart"].ToString(),
                                VbRestart = reader["VbRestart"].ToString(),
                                Wbs = reader["Wbs"].ToString(),
                                ICAgreement = reader["ICAgreement"].ToString(),
                                MeterSn = reader["Matricola Contatore"].ToString(),
                                MeterBrand = reader["Marca Contatore"].ToString(),
                                DeviceSn = reader["Matricola Convertitore"].ToString(),
                                DeviceBrand = reader["Marca convertitore"].ToString(),
                                DeviceModel = reader["Modello Convertitore"].ToString(),
                                Activity = reader["Attività"].ToString()
                            };

                                //if (DateTime.TryParse(executiondateinput, out DateTime MadeDate))
                                //{
                                //    obj.ExecutionDate = MadeDate;
                                //}
                                //var mDate = MadeDate.Date;
                                //if (mDate >= dFrom && mDate <= dTo)
                                //{
                                    list.Add(obj);
                                //}
                                
                            //}
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
            OrderedList =
            /*return*/ list.OrderBy(x => x.Customer).ToList();
            return OrderedList;
        }

        public async Task<user> UserMeta(string pmUSer)
        {
            var msg = new user();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spUserSector"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.uid = ds.Tables[0].Rows[0][0].ToString();
                            msg.sector = ds.Tables[0].Rows[0][1].ToString();
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception UserMeta: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> UserRole(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spUserRole"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spUserRole: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> CheckListPM(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spCheckListPM"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spCleckListPM: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> CheckListRAS(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spCheckListRAS"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spCleckListRAS: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> CheckListDGCDAQSAE(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spCheckListDGCDAQSAE"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spCheckListDGCDAQSAE: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> CplAree(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spArea"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spArea: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> CplAreeAll(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spAreaAll"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spAreaAll: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<dataToService> CplHeadAll(string pmUSer)
        {
            var msg = new dataToService();

            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spHeadAll"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = pmUSer;

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;
                        DataSet ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            msg.isSuccess = true;
                            msg.ds = ds;
                        }
                        else
                        {
                            msg.isSuccess = false;
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                msg.isSuccess = false;
                msg.response = ex.ToString();
                log.Error("Exception spHeadAll: {0}" + ex.ToString());
            }

            return msg;
        }
        public async Task<IList<PlantModel>> ListPlant(string code)
        {
            List<PlantModel> list = new List<PlantModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetPlantList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            PlantModel obj = new PlantModel();
                            obj.ElementCode = reader["ElementCode"].ToString();
                            obj.ElementName = reader["ElementName"].ToString();
                            obj.CustomerID = int.Parse(reader["CustomerID"].ToString());
                            obj.CustomerDescription = reader["CustomerDescription"].ToString();
                            obj.ElementAddress = reader["ElementAddress"].ToString();
                            obj.ElementStreetNumber = reader["ElementStreetNumber"].ToString();
                            obj.ElementZipCode = reader["ElementZipCode"].ToString();
                            obj.ElementCity = reader["ElementCity"].ToString();
                            obj.ElementPrv = reader["ElementPrv"].ToString();
                            obj.ElementRegion = reader["ElementRegion"].ToString();
                            obj.ElementState = reader["ElementState"].ToString();
                            obj.ElementLat = reader["ElementLatitude"].ToString();
                            obj.ElementLng = reader["ElementLongitude"].ToString();
                            obj.Sn = reader["Sn"].ToString();
                            obj.SnCont = reader["SnCont"].ToString();
                            obj.ModConv = reader["DescVc"].ToString();
                            obj.ModCont = reader["ModCont"].ToString();
                            obj.Stato = reader["Stato"].ToString();
                            obj.MarcaConv = reader["MARCA_VC"].ToString();
                            obj.MarcaCont = reader["MARCA_CONT"].ToString();
                            obj.Sim = reader["SIM_ECOR"].ToString();
                            obj.IdDeviceTypology = reader["IdDeviceTypology"].ToString();
                            obj.Remi = reader["REMI"].ToString();
                            obj.IdSr = reader["ID_SR"].ToString();
                            obj.DescSr = reader["DESC_SOTTORETE"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<PlantModel>> ListPlant(string type, string value)
        {
            List<PlantModel> list = new List<PlantModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetPlantListFilter"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@"+type, SqlDbType.NVarChar).Value = value;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            PlantModel obj = new PlantModel();
                            obj.ElementCode = reader["ElementCode"].ToString();
                            obj.ElementName = reader["ElementName"].ToString();
                            obj.CustomerID = int.Parse(reader["CustomerID"].ToString());
                            obj.CustomerDescription = reader["CustomerDescription"].ToString();
                            obj.ElementAddress = reader["ElementAddress"].ToString();
                            obj.ElementStreetNumber = reader["ElementStreetNumber"].ToString();
                            obj.ElementZipCode = reader["ElementZipCode"].ToString();
                            obj.ElementCity = reader["ElementCity"].ToString();
                            obj.ElementPrv = reader["ElementPrv"].ToString();
                            obj.ElementRegion = reader["ElementRegion"].ToString();
                            obj.ElementState = reader["ElementState"].ToString();
                            obj.ElementLat = reader["ElementLatitude"].ToString().Replace(",", ".");
                            obj.ElementLng = reader["ElementLongitude"].ToString().Replace(",", ".");
                            obj.Sn = reader["Sn"].ToString();
                            obj.SnCont = reader["SnCont"].ToString();
                            obj.ModConv = reader["DescVc"].ToString();
                            obj.ModCont = reader["ModCont"].ToString();
                            obj.Stato = reader["Stato"].ToString();
                            obj.MarcaConv = reader["MARCA_VC"].ToString();
                            obj.MarcaCont = reader["MARCA_CONT"].ToString();
                            obj.Sim = reader["SIM_ECOR"].ToString();
                            obj.IdDeviceTypology = reader["IdDeviceTypology"].ToString();
                            obj.Remi = reader["REMI"].ToString();
                            obj.IdSr = reader["ID_SR"].ToString();
                            obj.DescSr = reader["DESC_SOTTORETE"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<PlantModel>> ListPlantCode()
        {
            List<PlantModel> list = new List<PlantModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetPlantListCode"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            PlantModel obj = new PlantModel();
                            obj.ElementCode = reader["ElementCode"].ToString();
                            obj.ElementName = reader["ElementName"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<CustomerModel>> ListCustomer()
        {
            List<CustomerModel> list = new List<CustomerModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spListDistinctCustomer"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            CustomerModel obj = new CustomerModel();
                            obj.CustomerID = int.Parse(reader["CustomerID"].ToString());
                            obj.CustomerDescription = reader["CustomerDescription"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<CustomerModel>> ListCustomerEnrollment()
        {
            List<CustomerModel> list = new List<CustomerModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "select * from tbl_CUSTOMER";

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            CustomerModel obj = new CustomerModel();
                            obj.CustomerID = int.Parse(reader["ID_AZIENDA"].ToString());
                            obj.CustomerDescription = reader["DESC_AZIENDA"].ToString();
                            obj.PartitaIva = reader["PartitaIva"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<ICModel>> ListIC()
        {
            List<ICModel> list = new List<ICModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spListIC"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            ICModel obj = new ICModel();
                            obj.IC = reader["ICID"].ToString();
                            obj.ICDescription = reader["ICDescription"].ToString();
                            obj.CustomerID = int.Parse(reader["CustomerID"].ToString());
                            obj.Customer = reader["CustomerDescription"].ToString();
                            obj.Wbs = reader["WBSID"].ToString();
                            obj.WbsDescription = reader["WbsDefinition"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<AppNews>> ListNews(string user)
        {
            List<AppNews> list = new List<AppNews>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spAppNews"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@user", SqlDbType.NVarChar).Value = user;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            AppNews obj = new AppNews();
                            obj.CLCount = reader["clcount"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<WorkOrderModel>> WorkOrderList()
        {
            List<WorkOrderModel> list = new List<WorkOrderModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetWOList_test"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            string assigneddateinput = reader["ScheduledDate"].ToString();
                            DateTime ScheduledDate;

                            string executiondateinput = reader["ExecutionDate"].ToString();
                            DateTime ExecutionDate;

                            string cancelleddateinput = reader["CancelledDate"].ToString();
                            DateTime CancelledDate;

                            string CreatedDateDb = reader["CreatedDate"].ToString();
                            DateTime CreatedDate;

                            string reasoncodeinput = reader["ReasonCode"].ToString();
                            int ReasonCode;
                            string wormaninput = reader["WorkmanID"].ToString();
                            int WOID;

                            string WOStateDb = reader["WorkOrderStateID"].ToString();
                            int WOState;

                            WorkOrderModel obj = new WorkOrderModel();
                            obj.WOID = reader["WorkOrderID"].ToString();
                            obj.WOKey = reader["WorkOrderID"].ToString();
                            obj.WOElement = reader["ElementCode"].ToString();
                            obj.WOTypeID = int.Parse(reader["WorkOrderTypeID"].ToString());
                            obj.WOType = reader["WorkOrderType"].ToString();
                            obj.WOStateDescription = reader["WorkOrderState"].ToString();
                            obj.WOCID = int.Parse(reader["CustomerID"].ToString());
                            obj.WOCustomer = reader["CustomerDescription"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.CancelledBy = reader["CancelledBy"].ToString();
                            obj.AssignedTo = reader["AssignedTo"].ToString();
                            obj.ReasonText = reader["ReasonText"].ToString();
                            obj.wbs = reader["Wbs"].ToString();
                            obj.ic = reader["IC"].ToString();
                            obj.ReasonText = reader["ReasonText"].ToString();
                            obj.Activity = reader["Attività"].ToString();
                            obj.Esito = reader["Esito"].ToString();
                            obj.IsActive = reader["isACTIVE"].ToString();

                            if (int.TryParse(wormaninput, out WOID))
                            {
                                obj.WorkmanID = WOID;
                            }
                            else
                            {
                                obj.WorkmanID = null;
                            }
                            if (int.TryParse(WOStateDb, out WOState))
                            {
                                obj.WOState = WOState;
                            }
                            else
                            {
                                obj.WOState = null;
                            }
                            if (DateTime.TryParse(executiondateinput, out ExecutionDate))
                            {
                                obj.ExecutionDate = ExecutionDate;
                            }
                            else
                            {
                                obj.ExecutionDate = null;
                            }
                            if (DateTime.TryParse(CreatedDateDb, out CreatedDate))
                            {
                                obj.CreatedDate = CreatedDate;
                            }
                            else
                            {
                                obj.CreatedDate = null;
                            }

                            if (DateTime.TryParse(cancelleddateinput, out CancelledDate))
                            {
                                obj.CancelledDate = CancelledDate;
                            }
                            else
                            {
                                obj.CancelledDate = null;
                            }

                            if (DateTime.TryParse(assigneddateinput, out ScheduledDate))
                            {
                                obj.ScheduledDate = ScheduledDate;
                                obj.ScheduledDateEnd = ScheduledDate;
                            }
                            else
                            {
                                obj.ScheduledDate = null;
                                obj.ScheduledDateEnd = null;
                            }

                            if (int.TryParse(reasoncodeinput, out ReasonCode))
                            {
                                obj.ReasonCode = ReasonCode;
                            }
                            else
                            {
                                obj.ReasonCode = null;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        public async Task<IList<WorkOrderModel>> WorkOrderList(string code)
        {
            List<WorkOrderModel> list = new List<WorkOrderModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetWOList"))
                    {
                        command.Connection = cn1; command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            string assigneddateinput = reader["ScheduledDate"].ToString();
                            DateTime ScheduledDate;

                            string executiondateinput = reader["ExecutionDate"].ToString();
                            DateTime ExecutionDate;

                            string cancelleddateinput = reader["CancelledDate"].ToString();
                            DateTime CancelledDate;

                            string CreatedDateDb = reader["CreatedDate"].ToString();
                            DateTime CreatedDate;

                            string reasoncodeinput = reader["ReasonCode"].ToString();
                            int ReasonCode;
                            string wormaninput = reader["WorkmanID"].ToString();
                            int WOID;

                            string WOStateDb = reader["WorkOrderStateID"].ToString();
                            int WOState;

                            WorkOrderModel obj = new WorkOrderModel();
                            obj.WOID = reader["WorkOrderID"].ToString();
                            obj.WOKey = reader["WorkOrderID"].ToString();
                            obj.WOElement = reader["ElementCode"].ToString();
                            obj.WOTypeID = int.Parse(reader["WorkOrderTypeID"].ToString());
                            obj.WOType = reader["WorkOrderType"].ToString();
                            obj.WOStateDescription = reader["WorkOrderState"].ToString();
                            obj.WOCID = int.Parse(reader["CustomerID"].ToString());
                            obj.WOCustomer = reader["CustomerDescription"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.CancelledBy = reader["CancelledBy"].ToString();
                            obj.AssignedTo = reader["AssignedTo"].ToString();
                            obj.ReasonText = reader["ReasonText"].ToString();
                            obj.wbs = reader["Wbs"].ToString();
                            obj.ic = reader["IC"].ToString();
                            obj.ReasonText = reader["ReasonText"].ToString();
                            obj.Activity = reader["Attività"].ToString();
                            obj.Esito = reader["Esito"].ToString();
                            obj.IsActive = reader["isACTIVE"].ToString();

                            if (int.TryParse(wormaninput, out WOID))
                            {
                                obj.WorkmanID = WOID;
                            }
                            else
                            {
                                obj.WorkmanID = null;
                            }
                            if (int.TryParse(WOStateDb, out WOState))
                            {
                                obj.WOState = WOState;
                            }
                            else
                            {
                                obj.WOState = null;
                            }
                            if (DateTime.TryParse(executiondateinput, out ExecutionDate))
                            {
                                obj.ExecutionDate = ExecutionDate;
                            }
                            else
                            {
                                obj.ExecutionDate = null;
                            }
                            if (DateTime.TryParse(CreatedDateDb, out CreatedDate))
                            {
                                obj.CreatedDate = CreatedDate;
                            }
                            else
                            {
                                obj.CreatedDate = null;
                            }

                            if (DateTime.TryParse(cancelleddateinput, out CancelledDate))
                            {
                                obj.CancelledDate = CancelledDate;
                            }
                            else
                            {
                                obj.CancelledDate = null;
                            }

                            if (DateTime.TryParse(assigneddateinput, out ScheduledDate))
                            {
                                obj.ScheduledDate = ScheduledDate;
                                obj.ScheduledDateEnd = ScheduledDate;
                            }
                            else
                            {
                                obj.ScheduledDate = null;
                                obj.ScheduledDateEnd = null;
                            }

                            if (int.TryParse(reasoncodeinput, out ReasonCode))
                            {
                                obj.ReasonCode = ReasonCode;
                            }
                            else
                            {
                                obj.ReasonCode = null;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }

        //public async Task<IList<WorkOrderModel>> WorkOrderList(string code)
        //{
        //    List<WorkOrderModel> list = new List<WorkOrderModel>();
        //    try
        //    {
        //        using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
        //        {
        //            await cn1.OpenAsync();
        //            using (SqlCommand command = new SqlCommand("slt_GetWOList"))
        //            {
        //                command.Connection = cn1;command.CommandTimeout = 300;
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

        //                var reader = await command.ExecuteReaderAsync();

        //                while (await reader.ReadAsync())
        //                {
        //                    string assigneddateinput = reader["ScheduledDate"].ToString();
        //                    DateTime ScheduledDate;

        //                    string executiondateinput = reader["ExecutionDate"].ToString();
        //                    DateTime ExecutionDate;

        //                    string cancelleddateinput = reader["CancelledDate"].ToString();
        //                    DateTime CancelledDate;

        //                    string CreatedDateDb = reader["CreatedDate"].ToString();
        //                    DateTime CreatedDate;

        //                    string reasoncodeinput = reader["ReasonCode"].ToString();
        //                    int ReasonCode;
        //                    string wormaninput = reader["WorkmanID"].ToString();
        //                    int WOID;

        //                    string WOStateDb = reader["WorkOrderStateID"].ToString();
        //                    int WOState;

        //                    WorkOrderModel obj = new WorkOrderModel();
        //                    obj.WOID = reader["WorkOrderID"].ToString();
        //                    obj.WOKey = reader["WorkOrderID"].ToString();
        //                    obj.WOElement = reader["ElementCode"].ToString();
        //                    obj.WOTypeID = int.Parse(reader["WorkOrderTypeID"].ToString());
        //                    obj.WOType = reader["WorkOrderType"].ToString();
        //                    obj.WOStateDescription = reader["WorkOrderState"].ToString();
        //                    obj.WOCID = int.Parse(reader["CustomerID"].ToString());
        //                    obj.WOCustomer = reader["CustomerDescription"].ToString();
        //                    obj.CreatedBy = reader["CreatedBy"].ToString();
        //                    obj.CancelledBy = reader["CancelledBy"].ToString();
        //                    obj.AssignedTo = reader["AssignedTo"].ToString();
        //                    obj.ReasonText = reader["ReasonText"].ToString();
        //                    obj.wbs = reader["Wbs"].ToString();
        //                    obj.ic = reader["IC"].ToString();
        //                    obj.ReasonText = reader["ReasonText"].ToString();
        //                    obj.Activity = reader["Attività"].ToString();
        //                    obj.Esito = reader["Esito"].ToString();
        //                    if (int.TryParse(wormaninput, out WOID))
        //                    {
        //                        obj.WorkmanID = WOID;
        //                    }
        //                    else
        //                    {
        //                        obj.WorkmanID = null;
        //                    }
        //                    if (int.TryParse(WOStateDb, out WOState))
        //                    {
        //                        obj.WOState = WOState;
        //                    }
        //                    else
        //                    {
        //                        obj.WOState = null;
        //                    }
        //                    if (DateTime.TryParse(executiondateinput, out ExecutionDate))
        //                    {
        //                        obj.ExecutionDate = ExecutionDate;
        //                    }
        //                    else
        //                    {
        //                        obj.ExecutionDate = null;
        //                    }
        //                    if (DateTime.TryParse(CreatedDateDb, out CreatedDate))
        //                    {
        //                        obj.CreatedDate = CreatedDate;
        //                    }
        //                    else
        //                    {
        //                        obj.CreatedDate = null;
        //                    }

        //                    if (DateTime.TryParse(cancelleddateinput, out CancelledDate))
        //                    {
        //                        obj.CancelledDate = CancelledDate;
        //                    }
        //                    else
        //                    {
        //                        obj.CancelledDate = null;
        //                    }

        //                    if (DateTime.TryParse(assigneddateinput, out ScheduledDate))
        //                    {
        //                        obj.ScheduledDate = ScheduledDate;
        //                        obj.ScheduledDateEnd = ScheduledDate;
        //                    }
        //                    else
        //                    {
        //                        obj.ScheduledDate = null;
        //                        obj.ScheduledDateEnd = null;
        //                    }

        //                    if (int.TryParse(reasoncodeinput, out ReasonCode))
        //                    {
        //                        obj.ReasonCode = ReasonCode;
        //                    }
        //                    else
        //                    {
        //                        obj.ReasonCode = null;
        //                    }
        //                    list.Add(obj);
        //                }
        //            }
        //            cn1.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message, ex);
        //    }

        //    return list;
        //}

        public async Task<IList<WorkOrderDetailsModel>> WorkOrderDetailsList()
        {
            List<WorkOrderDetailsModel> list = new List<WorkOrderDetailsModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spWorkOrderDetailsList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            WorkOrderDetailsModel obj = new WorkOrderDetailsModel();
                            obj.WOID = int.Parse(reader["WOID"].ToString());
                            obj.WorkOrderDetails = reader["WorkOrderDetails"].ToString();
                            obj.MaterialsCode = reader["MaterialsCode"].ToString();
                            obj.MaterialsDescription = reader["MaterialsDescription"].ToString();
                            obj.Price = reader["Price"].ToString();
                            if (int.TryParse(reader["Qty"].ToString(), out var intQty))
                            {
                                obj.Qty = intQty;
                            }
                            else
                            {
                                obj.Qty = null;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderTypeModel>> WorkOrderTypeList()
        {
            List<WorkOrderTypeModel> list = new List<WorkOrderTypeModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spWorkOrderTypeList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            WorkOrderTypeModel obj = new WorkOrderTypeModel();
                            obj.WorkOrderTypeID = int.Parse(reader["WorkOrderTypeID"].ToString());
                            obj.WorkOrderType = reader["WorkOrderType"].ToString();

                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderInvoiceModel>> WorkOrder4InvoiceList()
        {
            List<WorkOrderInvoiceModel> list = new List<WorkOrderInvoiceModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spWorkOrder4InvoicesList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            string assigneddateinput = reader["ScheduledDate"].ToString();
                            DateTime ScheduledDate;

                            string executeddateinput = reader["ExecutionDate"].ToString();
                            DateTime ExecutionDate;

                            string htinput = reader["HoursTravelling"].ToString();
                            float HoursTravelling;

                            string hwinput = reader["HoursWorkman"].ToString();
                            float HoursWorkman;

                            string reasoncodeinput = reader["ReasonCode"].ToString();
                            int ReasonCode;
                            string wormaninput = reader["WorkmanID"].ToString();
                            int WOID;

                            WorkOrderInvoiceModel obj = new WorkOrderInvoiceModel();
                            obj.WOID = reader["WorkOrderID"].ToString();
                            obj.WOElement = reader["ElementCode"].ToString();
                            obj.WOTID = int.Parse(reader["WorkOrderTypeID"].ToString());
                            obj.WOType = reader["WorkOrderType"].ToString();
                            obj.WOSID = int.Parse(reader["WorkOrderStateID"].ToString());
                            obj.WOStateDescription = reader["WorkOrderState"].ToString();
                            obj.WOCID = int.Parse(reader["CustomerID"].ToString());
                            obj.WOCustomer = reader["CustomerDescription"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                            obj.AssignedTo = reader["AssignedTo"].ToString();
                            obj.ReasonText = reader["ReasonText"].ToString();
                            obj.CustomerOrder = reader["CustomerOrder"].ToString();
                            obj.wbs = reader["Wbs"].ToString();
                            obj.ic = reader["IC"].ToString();

                            if (int.TryParse(wormaninput, out WOID))
                            {
                                obj.WorkmanID = WOID;
                            }
                            else
                            {
                                obj.WorkmanID = null;
                            }

                            if (float.TryParse(htinput, out HoursTravelling))
                            {
                                obj.HoursTravelling = HoursTravelling;
                            }
                            else
                            {
                                obj.HoursTravelling = null;
                            }

                            if (float.TryParse(hwinput, out HoursWorkman))
                            {
                                obj.HoursWorkman = HoursWorkman;
                            }
                            else
                            {
                                obj.HoursWorkman = null;
                            }

                            if (DateTime.TryParse(assigneddateinput, out ScheduledDate))
                            {
                                obj.ScheduledDate = ScheduledDate;
                                obj.ScheduledDateEnd = ScheduledDate;
                            }
                            else
                            {
                                obj.ScheduledDate = null;
                                obj.ScheduledDateEnd = null;
                            }

                            if (DateTime.TryParse(executeddateinput, out ExecutionDate))
                            {
                                obj.ExecutionDate = ExecutionDate;
                            }
                            else
                            {
                                obj.ExecutionDate = null;
                            }

                            if (int.TryParse(reasoncodeinput, out ReasonCode))
                            {
                                obj.ReasonCode = ReasonCode;
                            }
                            else
                            {
                                obj.ReasonCode = null;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderStatusModel>> WorkOrderStatusList()
        {
            List<WorkOrderStatusModel> list = new List<WorkOrderStatusModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spWorkOrderStatusList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            WorkOrderStatusModel obj = new WorkOrderStatusModel();
                            obj.WorkOrderStatusID = int.Parse(reader["WorkOrderStateID"].ToString());
                            obj.WorkOrderStatus = reader["WorkOrderState"].ToString();

                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkmanModel>> WorkmanList()
        {
            List<WorkmanModel> list = new List<WorkmanModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spWorkmanList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            WorkmanModel obj = new WorkmanModel();
                            obj.WorkmanID = int.Parse(reader["EmployeeID"].ToString());
                            obj.WorkmanName = reader["EmployeeName"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<SectorModel>> SctorTypeList()
        {
            List<SectorModel> list = new List<SectorModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spSectorList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            SectorModel obj = new SectorModel();
                            obj.SectorTypeId = reader["SectorTypeID"].ToString();
                            obj.SectorType = reader["SectorType"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WbsrModel>> WbsList()
        {
            List<WbsrModel> list = new List<WbsrModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spWbsList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            WbsrModel obj = new WbsrModel();
                            obj.Wbs = reader["WBS"].ToString();
                            obj.CustomerId = int.Parse(reader["CustomerId"].ToString());
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<PriceListrModel>> PriceList()
        {
            List<PriceListrModel> list = new List<PriceListrModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spPriceList"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            PriceListrModel obj = new PriceListrModel();
                            obj.PriceListId = int.Parse(reader["PriceListId"].ToString());
                            obj.CustomerId = int.Parse(reader["CustomerId"].ToString());
                            obj.Item = reader["Item"].ToString();
                            obj.ItemPrice = reader["ItemPrice"].ToString();
                            obj.Wbs = reader["Wbs"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderDetails>> WorkOrderDetails(int odl)
        {
            List<WorkOrderDetails> list = new List<WorkOrderDetails>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_GetWODetails"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@IdOdl", SqlDbType.Int).Value = odl;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            WorkOrderDetails obj = new WorkOrderDetails();
                            obj.woid = int.Parse(reader["woid"].ToString());
                            obj.Timestamp = DateTime.Parse(reader["timestamp"].ToString());
                            obj.Activity = reader["Activity"].ToString();
                            obj.Solution = reader["Solution"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderWFMModel>> WorkOrderListWFMtest()
        {
            List<WorkOrderWFMModel> list = new List<WorkOrderWFMModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("_slt_queryCustomGetOdl"))
                    {

                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            DateTime oDate;
                            DateTime eDate;

                            WorkOrderWFMModel obj = new WorkOrderWFMModel();
                            obj.Odl = int.Parse(reader["ID_MAINTENANCE"].ToString());
                            obj.WorkOrderType = reader["DESC_ODL"].ToString();
                            obj.Pdr = reader["DESC_PDR"].ToString();
                            obj.SnDevice = reader["SN_ECOR"].ToString();
                            obj.DeviceModel = reader["TipoCorrettore"].ToString();
                            obj.Fw = reader["FW_ver"].ToString();
                            obj.CustomerName = reader["DESC_AZIENDA"].ToString();
                            obj.Network = reader["DESC_SOTTORETE"].ToString();
                            obj.PlantName = reader["DESC_PLANT"].ToString();
                            obj.Address = reader["VIA_PLANT"].ToString() + ", " + reader["CIV_PLANT"].ToString();
                            obj.City = reader["CITY_PLANT"].ToString();
                            obj.Province = reader["PROV_PLANT"].ToString();
                            obj.ZipCode = reader["CAP_PLANT"].ToString();
                            obj.UcStop = reader["UcStop"].ToString();
                            obj.VmStop = reader["VmStop"].ToString();
                            obj.VbStop = reader["VbStop"].ToString();
                            obj.UcRestart = reader["UcRestart"].ToString();
                            obj.VmRestart = reader["VmRestart"].ToString();
                            obj.VbRestart = reader["VbRestart"].ToString();
                            obj.Note = reader["Attività"].ToString();
                            obj.WorkerMan = reader["Manutentore"].ToString();
                            obj.Stato = reader["STATO_ODL"].ToString();
                            obj.Sostituzione = reader["Sostituzione"].ToString();
                            obj.isThereVerification = reader["VerificaPeriodica"].ToString();
                            obj.VerificationResult = reader["Esito"].ToString();
                            obj.ReportName = reader["ReportName"].ToString();
                            if (DateTime.TryParse(reader["DATE"].ToString(), out oDate))
                            {
                                obj.OpenDate = oDate;
                            }
                            else
                            {
                                obj.OpenDate = null;
                            }
                            if (DateTime.TryParse(reader["DATA_INTERVENTO"].ToString(), out eDate))
                            {
                                obj.ExecutionDate = eDate;
                            }
                            else
                            {
                                obj.ExecutionDate = null;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderWFMModel>> WorkOrderListWFM(DateTime from, DateTime to)
        {
            List<WorkOrderWFMModel> list = new List<WorkOrderWFMModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();

                    //SqlCommand cmdArtih = new SqlCommand("SET ARITHABORT ON", cn1);
                    //cmdArtih.ExecuteNonQuery();                                    

                    using (SqlCommand command = new SqlCommand("slt_queryArchiveGetOdl"))
                    {
                        command.Connection = cn1;
                        command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@dataFrom", SqlDbType.DateTime).Value = from;
                        command.Parameters.Add("@dataTo", SqlDbType.DateTime).Value = to;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            DateTime oDate;
                            DateTime eDate;

                            WorkOrderWFMModel obj = new WorkOrderWFMModel();
                            obj.Odl = int.Parse(reader["ID_MAINTENANCE"].ToString());
                            obj.WorkOrderType = reader["DESC_ODL"].ToString();
                            obj.Pdr = reader["DESC_PDR"].ToString();
                            obj.SnMeter = reader["SnMeter"].ToString();
                            obj.BrandMeter = reader["MeterBrand"].ToString();
                            obj.SnDevice = reader["SN_ECOR"].ToString();
                            obj.DeviceModel = reader["TipoCorrettore"].ToString();
                            obj.Fw = reader["FW_ver"].ToString();
                            obj.CustomerName = reader["DESC_AZIENDA"].ToString();
                            obj.Network = reader["DESC_SOTTORETE"].ToString();
                            obj.PlantName = reader["DESC_PLANT"].ToString();
                            obj.Address = reader["VIA_PLANT"].ToString() + ", " + reader["CIV_PLANT"].ToString();
                            obj.City = reader["CITY_PLANT"].ToString();
                            obj.Province = reader["PROV_PLANT"].ToString();
                            obj.ZipCode = reader["CAP_PLANT"].ToString();
                            obj.UcStop = reader["UcStop"].ToString();
                            obj.VmStop = reader["VmStop"].ToString();
                            obj.VbStop = reader["VbStop"].ToString();
                            obj.UcRestart = reader["UcRestart"].ToString();
                            obj.VmRestart = reader["VmRestart"].ToString();
                            obj.VbRestart = reader["VbRestart"].ToString();
                            obj.Note = reader["Attività"].ToString();
                            obj.WorkerMan = reader["Manutentore"].ToString();
                            obj.Stato = reader["STATO_ODL"].ToString();
                            obj.Sostituzione = reader["Sostituzione"].ToString();
                            obj.isThereVerification = reader["VerificaPeriodica"].ToString();
                            obj.VerificationResult = reader["Esito"].ToString();
                            obj.ReportName = reader["ReportName"].ToString();
                            if (DateTime.TryParse(reader["DATE"].ToString(), out oDate))
                            {
                                obj.OpenDate = oDate;
                            }
                            else
                            {
                                obj.OpenDate = null;
                            }
                            if (DateTime.TryParse(reader["DATA_INTERVENTO"].ToString(), out eDate))
                            {
                                obj.ExecutionDate = eDate;
                            }
                            else
                            {
                                obj.ExecutionDate = null;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderWFMModel>> WorkOrderListWFMA()
        {
            List<WorkOrderWFMModel> list = new List<WorkOrderWFMModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("_slt_queryGetOdlActive"))
                    {

                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            DateTime oDate;
                            DateTime eDate;
                            bool _sendmit;

                            WorkOrderWFMModel obj = new WorkOrderWFMModel();
                            obj.Odl = int.Parse(reader["ID_MAINTENANCE"].ToString());
                            obj.WorkOrderType = reader["DESC_ODL"].ToString();
                            obj.Pdr = reader["DESC_PDR"].ToString();
                            obj.SnDevice = reader["SN_ECOR"].ToString();
                            obj.DeviceModel = reader["TipoCorrettore"].ToString();
                            obj.Fw = reader["FW_ver"].ToString();
                            obj.CustomerId = int.Parse(reader["ID_AZIENDA"].ToString());
                            obj.CustomerName = reader["DESC_AZIENDA"].ToString();
                            obj.Network = reader["DESC_SOTTORETE"].ToString();
                            obj.NetworkId = int.Parse(reader["ID_SR"].ToString());
                            obj.PlantName = reader["DESC_PLANT"].ToString();
                            obj.Address = reader["VIA_PLANT"].ToString() + ", " + reader["CIV_PLANT"].ToString();
                            obj.City = reader["CITY_PLANT"].ToString();
                            obj.Province = reader["PROV_PLANT"].ToString();
                            obj.ZipCode = reader["CAP_PLANT"].ToString();
                            obj.Note = reader["Attività"].ToString();
                            obj.WorkerMan = reader["Manutentore"].ToString();
                            obj.Stato = reader["STATO_ODL"].ToString();
                            obj.Sostituzione = reader["Sostituzione"].ToString();
                            obj.isThereVerification = reader["VerificaPeriodica"].ToString();
                            obj.VerificationResult = reader["Esito"].ToString();
                            obj.ReportName = reader["ReportName"].ToString();
                            obj.TypeOdlId = int.Parse(reader["ID_TYPE_ODL"].ToString());
                            if (DateTime.TryParse(reader["DATE"].ToString(), out oDate))
                            {
                                obj.OpenDate = oDate;
                            }
                            else
                            {
                                obj.OpenDate = null;
                            }
                            if (DateTime.TryParse(reader["DATA_INTERVENTO"].ToString(), out eDate))
                            {
                                obj.ExecutionDate = eDate;
                            }
                            else
                            {
                                obj.ExecutionDate = null;
                            }
                            if (bool.TryParse(reader["SENDMIT"].ToString(), out _sendmit))
                            {
                                obj.sendmit = _sendmit;
                            }
                            else
                            {
                                obj.sendmit = false;
                            }
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<IList<WorkOrderImgWFMModel>> WorkOrderImgListWFM(int odl)
        {
            List<WorkOrderImgWFMModel> list = new List<WorkOrderImgWFMModel>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_queryImg"))
                    {

                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@idOdl", SqlDbType.Int).Value = odl;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            WorkOrderImgWFMModel obj = new WorkOrderImgWFMModel();
                            obj.Odl = int.Parse(reader["ID_MAN"].ToString());
                            obj.Pdr = reader["DESC_PDR"].ToString();
                            obj.Name = reader["NAME_PHOTO"].ToString();
                            obj.Img = reader["PATH_PHOTO_TEMP"].ToString();
                            obj.Path = reader["PATH_PHOTO"].ToString();
                            obj.VmStop = reader["VmStop"].ToString();
                            obj.VbStop = reader["VbStop"].ToString();
                            obj.UCStop = reader["UCStop"].ToString();
                            obj.VmRestart = reader["VmRestart"].ToString();
                            obj.VbRestart = reader["VbRestart"].ToString();
                            obj.UCRestart = reader["UCRestart"].ToString();
                            obj.PhotoType = reader["PhotoType"].ToString();

                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return list;
        }
        public async Task<Mail> GetRecipient(int _azienda)
        {
            Mail obj = new Mail();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "select MAIL_REF from tbl_sr where id_sr = @idsr";
                        command.Parameters.Add("@idsr", SqlDbType.Int).Value = _azienda;
                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            obj.Recipient = reader["MAIL_REF"].ToString();
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            return obj;
        }
        public async Task<SysConfig> SysConfigMap()
        {
            SysConfig obj = new SysConfig();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("slt_getSysConfig"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            obj.CertificatePathElaborate = reader["CertificatePathElaborate"].ToString();
                            obj.CertificatePathToElaborate = reader["CertificatePathToElaborate"].ToString();
                            obj.PROSPECT_WS_URL = reader["PROSPECT_WS_URL"].ToString();
                            obj.ReportServer_URL = reader["ReportServer_URL"].ToString();
                            obj.MailNotificationSender = /*"supportmetering@cpl.it";*/ /*TODO : get email from db*/ reader["MailNotificationSender"].ToString();
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            return obj;
        }
        public async Task<IList<NgActivity>> NGActivities()
        {
            List<NgActivity> list = new List<NgActivity>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spGetNGActivity"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            NgActivity obj = new NgActivity();
                            obj.Odl = int.Parse(reader["Odl"].ToString());
                            obj.ActivityDate = DateTime.Parse(reader["DataOra"].ToString());
                            obj.PlantCode = reader["PlantCode"].ToString();
                            obj.Customer = reader["Customer"].ToString();
                            obj.Plant = reader["Plant"].ToString();
                            obj.PlantAddress = reader["PlantAddress"].ToString();
                            obj.LAT = reader["LAT"].ToString();
                            obj.LONG = reader["LONG"].ToString();
                            obj.TypeOdl = reader["TypeOdl"].ToString();
                            obj.Technician = reader["Technician"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            return list;
        }
        public async Task<IList<NgActivityStat>> NGActivitiesStat()
        {
            List<NgActivityStat> list = new List<NgActivityStat>();
            try
            {
                using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
                {
                    await cn1.OpenAsync();
                    using (SqlCommand command = new SqlCommand("spGetNGSummary"))
                    {
                        command.Connection = cn1;command.CommandTimeout = 300;
                        command.CommandType = CommandType.StoredProcedure;

                        var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {
                            NgActivityStat obj = new NgActivityStat();
                            obj.ActivitiesCount = int.Parse(reader["Conto"].ToString());
                            obj.ActivitiesAvg = int.Parse(reader["Media"].ToString());
                            obj.ActivitiesDescription = reader["Description"].ToString();
                            list.Add(obj);
                        }
                    }
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            return list;
        }
        #endregion

        #region "Insert"
        public async Task<DataResponse> _InsertWorkOrder(WorkOrderModel wom, string user)
        {
            using (SqlConnection cn1 = new SqlConnection(cString().ConnectionString))
            {
                await cn1.OpenAsync();
                using (SqlCommand cmd = new SqlCommand("spInsertWorkOrder"))
                {
                    cmd.Connection = cn1;
                    cmd.CommandType = CommandType.StoredProcedure;
                    var returnObject = new DataResponse();

                    SqlTransaction t;
                    t = cn1.BeginTransaction("Transazione spInsertWorkOrder");
                    cmd.Transaction = t;
                    try
                    {
                        cmd.Parameters.Add("@WOPK", SqlDbType.NVarChar).Value = wom.WOPK;
                        cmd.Parameters.Add("@WOTID", SqlDbType.Int).Value = wom.WOType;
                        cmd.Parameters.Add("@WOSID", SqlDbType.Int).Value = wom.WOState;
                        cmd.Parameters.Add("@DID", SqlDbType.Int).Value = wom.WODID;
                        cmd.Parameters.Add("@CID", SqlDbType.Int).Value = wom.WOCID;
                        cmd.Parameters.Add("@ElementCode", SqlDbType.NVarChar).Value = wom.WOElement;
                        cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = wom.CreatedBy;
                        cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = wom.CreatedDate;
                        cmd.Parameters.Add("@AssignedTo", SqlDbType.Int).Value = wom.AssignedTo;
                        cmd.Parameters.Add("@ScheduledDate", SqlDbType.DateTime).Value = wom.ScheduledDate;
                        cmd.Parameters.Add("@ReasonCode", SqlDbType.Int).Value = wom.ReasonCode;
                        cmd.Parameters.Add("@ReasonText", SqlDbType.NVarChar, 4000).Value = wom.ReasonText;
                        cmd.Parameters.Add("@wbs", SqlDbType.NVarChar).Value = wom.wbs;
                        cmd.Parameters.Add("@ic", SqlDbType.NVarChar).Value = wom.ic;
                        cmd.Parameters.Add("@customerorder", SqlDbType.NVarChar).Value = wom.CustomerOrder;
                        await cmd.ExecuteNonQueryAsync();
                        t.Commit();
                        returnObject.success = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error("Commit Exception spInsertWorkOrder: {0}" + ex.Message);
                        returnObject.success = false;
                        try
                        {
                            t.Rollback();
                            returnObject.success = false;
                            log.Error("COMMIT spInsertWorkOrder NOK, dettaglio: " + ex.Message + " FATTO ROLL-BACK, operazione di: " + user);
                            returnObject.response = "Fatto ROLL-BACK di spInsertWorkOrder " + ex.Message;
                        }
                        catch (Exception ex2)
                        {
                            log.Error("ROLLBACK spInsertWorkOrder NOK, dettaglio: " + ex2.Message + " IMPOSSIBILE FARE ROLL-BACK, operazione di: " + user);
                            returnObject.success = false;
                            returnObject.response = "Impossibile Fare ROLL-BACK di spInsertWorkOrder " + ex2.Message;
                        }
                    }
                    cmd.Connection.Close();
                    return returnObject;
                }
            }
        }
        #endregion

        #region "Close"
        public async Task<DataResponse> _CloseWorkOrder(WorkOrderClosing wom, string user)
        {
            using (SqlConnection cn1 = new SqlConnection(EnrollmentString().ConnectionString))
            {
                await cn1.OpenAsync();
                using (SqlCommand cmd = new SqlCommand("_slt_ConfirmOdl"))
                {
                    cmd.Connection = cn1;
                    cmd.CommandType = CommandType.StoredProcedure;
                    var returnObject = new DataResponse();

                    SqlTransaction t;
                    t = cn1.BeginTransaction("Transazione _slt_ConfirmOdl");
                    cmd.Transaction = t;
                    try
                    {
                        cmd.Parameters.Add("@pdr", SqlDbType.NVarChar).Value = wom.WOCode;
                        cmd.Parameters.Add("@id_odl", SqlDbType.Int).Value = wom.WOID;
                        cmd.Parameters.Add("@user", SqlDbType.NVarChar).Value = user;
                        cmd.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DateTime.Now;
                        await cmd.ExecuteNonQueryAsync();
                        t.Commit();
                        returnObject.success = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error("Commit Exception _update_CloseOdlMan: {0}" + ex.Message);
                        returnObject.success = false;
                        try
                        {
                            t.Rollback();
                            returnObject.success = false;
                            log.Error("COMMIT _update_CloseOdlMan NOK, dettaglio: " + ex.Message + " FATTO ROLL-BACK, operazione di: " + user);
                            returnObject.response = "Fatto ROLL-BACK di _update_CloseOdlMan " + ex.Message;
                        }
                        catch (Exception ex2)
                        {
                            log.Error("ROLLBACK _update_CloseOdlMan NOK, dettaglio: " + ex2.Message + " IMPOSSIBILE FARE ROLL-BACK, operazione di: " + user);
                            returnObject.success = false;
                            returnObject.response = "Impossibile Fare ROLL-BACK di _update_CloseOdlMan " + ex2.Message;
                        }
                    }
                    cmd.Connection.Close();
                    return returnObject;
                }
            }
        }

        #endregion

        DataAccessLayer()
        {

        }
    }

}
